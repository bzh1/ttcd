<!DOCTYPE html>

<style>
.headero {
  padding: 20px;
  text-align: center;
  background: grey;
  color: white;
}
.footer {
  padding: 20px;
  text-align: center;
  background: grey;
  }

    </style>
        <html lang="de-CH" class="no-js">
 
    <head>
        <title>SHTO KommissionI</title>
	<link rel="stylesheet" href="{{asset('css\app.css')}}">
        <link rel="stylesheet" href="{{asset('css\icons.css')}}">
       <meta name="robots" content="all,noodp">
          <meta name="viewport" content="width=device-width, initial-scale=1">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="application-name" content="Elektrogrosshandel">
            <meta name="theme-color" content="#0083C3">
            <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="csrf-token" content="k8E543y1c8pfewXIGW0kjeSwVS8ejVvAU6aiuCRSCiic0uXQcyvO8sXUDcXYhrAC">
	        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/static/images/favicon/apple-touch-icon-57x57.png">
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/images/favicon/apple-touch-icon-114x114.png">
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/images/favicon/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/images/favicon/apple-touch-icon-144x144.png">
            <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/static/images/favicon/apple-touch-icon-60x60.png">
            <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/images/favicon/apple-touch-icon-120x120.png">
            <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/images/favicon/apple-touch-icon-76x76.png">
            <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/images/favicon/apple-touch-icon-152x152.png">
            <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/favicon.ico">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-196x196.png" sizes="196x196">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-96x96.png" sizes="96x96">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-32x32.png" sizes="32x32">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-16x16.png" sizes="16x16">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-128.png" sizes="128x128">   
    <meta name="msapplication-config" content="/static/ieconfig.xml">
    <link rel="manifest" href="/static/manifest.webmanifest"> 
            <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=greek">    
            <link rel="stylesheet" type="text/css" href="/static/css/icons.css?f4a2080209ffae2cdba0e">
            <link rel="stylesheet" type="text/css" href="/static/css/app.css?f4a2080209ffae2cdba0e">      
            <script>              
                var cssLoaded = true;
            </script>
            <script defer src="/static/js/app.js?f4a2080209ffae2cdba0e"></script>
            <link rel="canonical" href="https://www.elektrogrosshandel.ch/de/costcenters/">               
                        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53269130-1"></script>                
                    <script>
                        window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                        gtag('config', 'UA-53269130-1');
                        
                    </script> 
    </head>
	   
<div class="headero">
  <h1>My Website</h1>
</div>
<header class="shopheader">
<section class="shopheader__middle">             
        </a>
    </div>

                    
                        <div class="shopheader__search">
                                <form class="input-group inline-button"  style=" float: right;" method="GET" role="search" action="{{route('search.create')}}">
                        @csrf
                        <input class="input-group-field" placeholder="Search" name="search" aria-label="Search" style="margin-top: 5px;">
                        <button class="button clear" type="submit" style="margin-top: 5px;">Search</button>
                      </form> 
                            <div class="shopheader__search-instant-results" data-instant-search-results></div>
                        </div>
                    
                    
                        
                      <section class="shopheader__account">                    
                                    <a href="{{ url('/home') }}" data-toggle="account-menu" title="Mein Konto">
                                        <i class="icon icon-user medium"></i>
                                    </a>
                                    



                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                                            
                                                            <i class="fa fa-sign-out" style="font-size:20px;color:red"></i>
                                                        

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                    </div>




                                      
                                    
                                    <div class="shopheader-menu shopheader-menu--account dropdown-pane widget"
                                         id="account-menu"
                                         data-dropdown
                                         data-hover="true"
                                         data-hover-pane="true"
                                         data-position="bottom"
                                         data-alignment="right">

                                        <div class="widget__heading">
                     <form method="POST" class="width-100" action="/de/account/logout/">
                                                    <input type="hidden" name="csrfmiddlewaretoken" value="QAtYp5OejKarMVrbXLYOQpQwNCxTCyugqyZbPE75JU3oyTrjtntiFDVUvWmDA4zi">
                                                    <button type="submit" class="button hollow margin-top-1 width-100">
                                                        <i class="icon icon-logout medium"></i>
                                                        <span>Abmelden</span>
                                                    </button>
                                                </form>
                                                
                                            
                                        </div>
                                    </div>
                                
                            
                            
                              
                                   <a
                                    href="{{ route('cart.index') }}"
                                    title=" "
                                    data-toggle="cart-menu"
                                    data-bind-prop-set="cart:activeCartLabel,title=Warenkorb {}">
                                
                                    <i class="icon icon-cart medium"></i>
                                    <a href="html.html">
                                    <span class="badge hide" data-bind-text="cart:activePositionCount" data-bind-class="cart:isActiveEmpty,hide">
                                      
                                    </span>
                                </a>
                                <div
                                    class="shopheader-menu shopheader-menu--cart dropdown-pane widget "
                                    
                                    data-bind-prop-set="cart:hasActivePositions,data-cart-preview=/de/cart/preview"
                                    data-cart-preview=/de/cart/preview
                                    id="cart-menu"
                                    data-dropdown
                                    data-hover="true"
                                    data-hover-pane="true"
                                    data-position="bottom"
                                    data-alignment="right"
                                >
                                    <div class="cart-preview" data-cart-preview-widget>
                                        <div class="cart-preview__loader"><i class="icon loader"></i></div>
                                        <div
                                            class="cart-preview__content"
                                            data-bind-innerhtml="cart:preview"
                                        ></div>
                                    </div>
                                </div>
                            
                        </section>
                </div>
            </section>
                                            </div>
                                                
                                            </div>
                                        </div>
            
                </div>
            </nav>
        
    </header> 

        <header class="print-header grid-x">
    
        <div class="cell small-5">
            <img src="/static/images/logo.svg" class="print-header__logo">
        </div>
    
    
        <div class="cell small-7">
            <div class="grid-x grid-margin-x">
                <div class="cell auto">
                  
                </div>
                   </div>
            </div>
        </div>
    
</header>

    

    
        <main id="app" class="bg-white">
            
    
   
    
    
    <section class="container medium padding-vertical-3">
        

    

    <form method="post" data-abide novalidate>

        <input type="hidden" name="csrfmiddlewaretoken" value="QAtYp5OejKarMVrbXLYOQpQwNCxTCyugqyZbPE75JU3oyTrjtntiFDVUvWmDA4zi">
        
    


        <h5 class="form__title">KommissionI</h5>
        <div class="grid-x grid-margin-x">
            <div class="cell tablet-6">
                
                    

    </form>

  
 <form action="{{route('commission.store')}}" method="POST">
          @csrf
          <div class="form-group pt-2">
            <label>Emri</label>
            <input @error('name') is-invalid @enderror class="form-control" name='name' id="name" maxlength="40" type="text" placeholder="">
          @error('name')
            <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
              </span>
          @enderror
          </div>
          <div class="form-group pt-2">
            <label>Pershkrimi</label>
            <input @error('desc') is-invalid @enderror class="form-control"name=' desc' id="desc" maxlength="37" type="text" placeholder="">
            @error('desc')
            <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
              </span>
          @enderror
          </div>
          <div class="form__footer">
            <div class="form__footer__col">
                <button class="button" type="submit">Shto</button>
                {{-- <button class="button" >Anulo</button> --}}
            </div>
          </div>
        </form> 

         </section>
         </main> 
    

        
    <footer class="shopfooter">
            <section class="container medium padding-top-1 padding-bottom-2">
                    <div class="grid-x grid-padding-y">
                    <div class="cell text-center">                             
            </section>
        
    </footer>

<div class="footer">
  <strong>&copy  THE THECH TERMS COMPUTER DIRECTORY</strong>
</div>

    </form>
    
    </body>
</html>
