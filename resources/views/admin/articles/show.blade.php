<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Mirrored from www.online-lighting-shop.com/eglo-49394-charterhouse by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 29 Mar 2020 22:35:39 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript">
    (window.NREUM || (NREUM = {})).loader_config = { licenseKey: "894ed55699", applicationID: "7200900" };
    window.NREUM || (NREUM = {}), __nr_require = function(e, n, t) {
        function r(t) { if (!n[t]) { var i = n[t] = { exports: {} };
                e[t][0].call(i.exports, function(n) { var i = e[t][1][n]; return r(i || n) }, i, i.exports) } return n[t].exports } if ("function" == typeof __nr_require) return __nr_require; for (var i = 0; i < t.length; i++) r(t[i]); return r }({ 1: [function(e, n, t) {
            function r() {}

            function i(e, n, t) { return function() { return o(e, [u.now()].concat(f(arguments)), n ? null : this, t), n ? void 0 : this } } var o = e("handle"),
                a = e(4),
                f = e(5),
                c = e("ee").get("tracer"),
                u = e("loader"),
                s = NREUM; "undefined" == typeof window.newrelic && (newrelic = s); var p = ["setPageViewName", "setCustomAttribute", "setErrorHandler", "finished", "addToTrace", "inlineHit", "addRelease"],
                l = "api-",
                d = l + "ixn-";
            a(p, function(e, n) { s[n] = i(l + n, !0, "api") }), s.addPageAction = i(l + "addPageAction", !0), s.setCurrentRouteName = i(l + "routeName", !0), n.exports = newrelic, s.interaction = function() { return (new r).get() }; var m = r.prototype = { createTracer: function(e, n) { var t = {},
                        r = this,
                        i = "function" == typeof n; return o(d + "tracer", [u.now(), e, t], r),
                        function() { if (c.emit((i ? "" : "no-") + "fn-start", [u.now(), r, i], t), i) try { return n.apply(this, arguments) } catch (e) { throw c.emit("fn-err", [arguments, this, e], t), e } finally { c.emit("fn-end", [u.now()], t) } } } };
            a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","), function(e, n) { m[n] = i(d + n) }), newrelic.noticeError = function(e, n) { "string" == typeof e && (e = new Error(e)), o("err", [e, u.now(), !1, n]) } }, {}], 2: [function(e, n, t) {
            function r(e, n) { var t = e.getEntries();
                t.forEach(function(e) { "first-paint" === e.name ? c("timing", ["fp", Math.floor(e.startTime)]) : "first-contentful-paint" === e.name && c("timing", ["fcp", Math.floor(e.startTime)]) }) }

            function i(e, n) { var t = e.getEntries();
                t.length > 0 && c("lcp", [t[t.length - 1]]) }

            function o(e) { if (e instanceof s && !l) { var n, t = Math.round(e.timeStamp);
                    n = t > 1e12 ? Date.now() - t : u.now() - t, l = !0, c("timing", ["fi", t, { type: e.type, fid: n }]) } } if (!("init" in NREUM && "page_view_timing" in NREUM.init && "enabled" in NREUM.init.page_view_timing && NREUM.init.page_view_timing.enabled === !1)) { var a, f, c = e("handle"),
                    u = e("loader"),
                    s = NREUM.o.EV; if ("PerformanceObserver" in window && "function" == typeof window.PerformanceObserver) { a = new PerformanceObserver(r), f = new PerformanceObserver(i); try { a.observe({ entryTypes: ["paint"] }), f.observe({ entryTypes: ["largest-contentful-paint"] }) } catch (p) {} } if ("addEventListener" in document) { var l = !1,
                        d = ["click", "keydown", "mousedown", "pointerdown", "touchstart"];
                    d.forEach(function(e) { document.addEventListener(e, o, !1) }) } } }, {}], 3: [function(e, n, t) {
            function r(e, n) { if (!i) return !1; if (e !== i) return !1; if (!n) return !0; if (!o) return !1; for (var t = o.split("."), r = n.split("."), a = 0; a < r.length; a++)
                    if (r[a] !== t[a]) return !1; return !0 } var i = null,
                o = null,
                a = /Version\/(\S+)\s+Safari/; if (navigator.userAgent) { var f = navigator.userAgent,
                    c = f.match(a);
                c && f.indexOf("Chrome") === -1 && f.indexOf("Chromium") === -1 && (i = "Safari", o = c[1]) } n.exports = { agent: i, version: o, match: r } }, {}], 4: [function(e, n, t) {
            function r(e, n) { var t = [],
                    r = "",
                    o = 0; for (r in e) i.call(e, r) && (t[o] = n(r, e[r]), o += 1); return t } var i = Object.prototype.hasOwnProperty;
            n.exports = r }, {}], 5: [function(e, n, t) {
            function r(e, n, t) { n || (n = 0), "undefined" == typeof t && (t = e ? e.length : 0); for (var r = -1, i = t - n || 0, o = Array(i < 0 ? 0 : i); ++r < i;) o[r] = e[n + r]; return o } n.exports = r }, {}], 6: [function(e, n, t) { n.exports = { exists: "undefined" != typeof window.performance && window.performance.timing && "undefined" != typeof window.performance.timing.navigationStart } }, {}], ee: [function(e, n, t) {
            function r() {}

            function i(e) {
                function n(e) { return e && e instanceof r ? e : e ? c(e, f, o) : o() }

                function t(t, r, i, o) { if (!l.aborted || o) { e && e(t, r, i); for (var a = n(i), f = v(t), c = f.length, u = 0; u < c; u++) f[u].apply(a, r); var p = s[y[t]]; return p && p.push([b, t, r, a]), a } }

                function d(e, n) { h[e] = v(e).concat(n) }

                function m(e, n) { var t = h[e]; if (t)
                        for (var r = 0; r < t.length; r++) t[r] === n && t.splice(r, 1) }

                function v(e) { return h[e] || [] }

                function g(e) { return p[e] = p[e] || i(t) }

                function w(e, n) { u(e, function(e, t) { n = n || "feature", y[t] = n, n in s || (s[n] = []) }) } var h = {},
                    y = {},
                    b = { on: d, addEventListener: d, removeEventListener: m, emit: t, get: g, listeners: v, context: n, buffer: w, abort: a, aborted: !1 }; return b }

            function o() { return new r }

            function a() {
                (s.api || s.feature) && (l.aborted = !0, s = l.backlog = {}) } var f = "nr@context",
                c = e("gos"),
                u = e(4),
                s = {},
                p = {},
                l = n.exports = i();
            l.backlog = s }, {}], gos: [function(e, n, t) {
            function r(e, n, t) { if (i.call(e, n)) return e[n]; var r = t(); if (Object.defineProperty && Object.keys) try { return Object.defineProperty(e, n, { value: r, writable: !0, enumerable: !1 }), r } catch (o) {}
                return e[n] = r, r } var i = Object.prototype.hasOwnProperty;
            n.exports = r }, {}], handle: [function(e, n, t) {
            function r(e, n, t, r) { i.buffer([e], r), i.emit(e, n, t) } var i = e("ee").get("handle");
            n.exports = r, r.ee = i }, {}], id: [function(e, n, t) {
            function r(e) { var n = typeof e; return !e || "object" !== n && "function" !== n ? -1 : e === window ? 0 : a(e, o, function() { return i++ }) } var i = 1,
                o = "nr@id",
                a = e("gos");
            n.exports = r }, {}], loader: [function(e, n, t) {
            function r() { if (!x++) { var e = E.info = NREUM.info,
                        n = d.getElementsByTagName("script")[0]; if (setTimeout(s.abort, 3e4), !(e && e.licenseKey && e.applicationID && n)) return s.abort();
                    u(y, function(n, t) { e[n] || (e[n] = t) }), c("mark", ["onload", a() + E.offset], null, "api"); var t = d.createElement("script");
                    t.src = "https://" + e.agent, n.parentNode.insertBefore(t, n) } }

            function i() { "complete" === d.readyState && o() }

            function o() { c("mark", ["domContent", a() + E.offset], null, "api") }

            function a() { return O.exists && performance.now ? Math.round(performance.now()) : (f = Math.max((new Date).getTime(), f)) - E.offset } var f = (new Date).getTime(),
                c = e("handle"),
                u = e(4),
                s = e("ee"),
                p = e(3),
                l = window,
                d = l.document,
                m = "addEventListener",
                v = "attachEvent",
                g = l.XMLHttpRequest,
                w = g && g.prototype;
            NREUM.o = { ST: setTimeout, SI: l.setImmediate, CT: clearTimeout, XHR: g, REQ: l.Request, EV: l.Event, PR: l.Promise, MO: l.MutationObserver }; var h = "" + location,
                y = { beacon: "bam.nr-data.net", errorBeacon: "bam.nr-data.net", agent: "js-agent.newrelic.com/nr-1167.min.js" },
                b = g && w && w[m] && !/CriOS/.test(navigator.userAgent),
                E = n.exports = { offset: f, now: a, origin: h, features: {}, xhrWrappable: b, userAgent: p };
            e(1), e(2), d[m] ? (d[m]("DOMContentLoaded", o, !1), l[m]("load", r, !1)) : (d[v]("onreadystatechange", i), l[v]("onload", r)), c("mark", ["firstbyte", f], null, "api"); var x = 0,
                O = e(6) }, {}], "wrap-function": [function(e, n, t) {
            function r(e) { return !(e && e instanceof Function && e.apply && !e[a]) } var i = e("ee"),
                o = e(5),
                a = "nr@original",
                f = Object.prototype.hasOwnProperty,
                c = !1;
            n.exports = function(e, n) {
                function t(e, n, t, i) {
                    function nrWrapper() { var r, a, f, c; try { a = this, r = o(arguments), f = "function" == typeof t ? t(r, a) : t || {} } catch (u) { l([u, "", [r, a, i], f]) } s(n + "start", [r, a, i], f); try { return c = e.apply(a, r) } catch (p) { throw s(n + "err", [r, a, p], f), p } finally { s(n + "end", [r, a, c], f) } } return r(e) ? e : (n || (n = ""), nrWrapper[a] = e, p(e, nrWrapper), nrWrapper) }

                function u(e, n, i, o) { i || (i = ""); var a, f, c, u = "-" === i.charAt(0); for (c = 0; c < n.length; c++) f = n[c], a = e[f], r(a) || (e[f] = t(a, u ? f + i : i, o, f)) }

                function s(t, r, i) { if (!c || n) { var o = c;
                        c = !0; try { e.emit(t, r, i, n) } catch (a) { l([a, t, r, i]) } c = o } }

                function p(e, n) { if (Object.defineProperty && Object.keys) try { var t = Object.keys(e); return t.forEach(function(t) { Object.defineProperty(n, t, { get: function() { return e[t] }, set: function(n) { return e[t] = n, n } }) }), n } catch (r) { l([r]) }
                    for (var i in e) f.call(e, i) && (n[i] = e[i]); return n }

                function l(n) { try { e.emit("internal-error", n) } catch (t) {} } return e || (e = i), t.inPlace = u, t.flag = a, t } }, {}] }, {}, ["loader"]);

    </script>
    <title>{{$article->name}}</title>
    <meta name="description" content="49394 from Eglo. Order quickly and reliably at the biggest online lighting store.
" />
    <meta name="keywords" content="49394,Eglo,, verlichting" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <link rel="icon" href="skin/frontend/assyrian/uk/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="skin/frontend/assyrian/uk/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />
    <script type="text/javascript">
    //<![CDATA[
    Mage.Cookies.path = 'index.html';
    Mage.Cookies.domain = '.www.online-lighting-shop.com';
    //]]>

    </script>
    <script type="text/javascript">
    //<![CDATA[
    optionalZipCountries = ["HK", "IE", "MO", "PA"];
    //]]>

    </script>
    <!-- BEGIN GOOGLE UNIVERSAL ANALYTICS CODE -->
    <script type="text/javascript">
    //<![CDATA[
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');


    ga('create', 'UA-74104491-1', 'auto');

    ga('send', 'pageview');

    //]]>

    </script>
    <!-- END GOOGLE UNIVERSAL ANALYTICS CODE -->
    <script type="text/javascript">
    (function(w, d, s, r, n) {
        w.TrustpilotObject = n;
        w[n] = w[n] || function() {
            (w[n].q = w[n].q || []).push(arguments) };
        a = d.createElement(s);
        a.async = 1;
        a.src = r;
        a.type = 'text/java' + s;
        f = d.getElementsByTagName(s)[0];
        f.parentNode.insertBefore(a, f)
    })(window, document, 'script', '../invitejs.trustpilot.com/tp.min.js', 'tp');
    tp('register', '');

    </script>
    <script type="text/javascript">
    //<![CDATA[
    var Translator = new Translate([]);
    //]]>

    </script>
    <meta name="google-site-verification" content="v8jZ9kdUj-R1kDZBqPlIFyBy9LYFcZKBQ2pzvJuNepI" />
    <script type="text/javascript">
    function rankingsPush() {
        var url = String(document.referrer);
        if (url.indexOf("google.nl") != -1) {
            var urlVars = {};
            var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
                urlVars[key] = value;
            });
            _gaq.push(['_setCustomVar', '1', 'Rankings', urlVars["cd"], 1]);
        }
    }

    </script>
    <script type="text/javascript">
    decorateTable('product-attribute-specs-table')

    </script>
    <script type="text/javascript">
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block") {
            ele.style.display = "none";
            text.innerHTML = "Meer info >";
        } else {
            ele.style.display = "block";
            text.innerHTML = "Verberg meer info >";
        }
    }

    </script>
    <script type="text/javascript" src="../widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js"></script>
</head>

<body class=" catalog-product-view catalog-product-view product-eglo-49394-charterhouse">
    <!-- Google Tag Manager -->
    <script type="text/javascript">
    dataLayer = [{
        "pageCategory": "product-detail"
    }];

    </script>
    <script type="text/javascript">
    dataLayer.push({
        "ecommerce": {
            "detail": {
                "actionField": {
                    "list": "Catalog"
                },
                "products": {
                    "name": "{{$article->name}}",
                    "id": "49394",
                    "price": "62.95",
                    "category": "Wall Lamps"
                }
            }
        }
    });

    </script>
    <script type="text/javascript">
    function gtmDataBuilder(gtmData) { dataLayer.push(gtmData); return true; }

    </script> <noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-MG56X5" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MG56X5');</script>
    <!-- End Google Tag Manager -->
    <div class="wrapper">
        <noscript>
            <div class="global-site-notice noscript">
                <div class="notice-inner">
                    <p>
                        <strong>JavaScript seems to be disabled in your browser.</strong><br />
                        You must have JavaScript enabled in your browser to utilize the functionality of this website. </p>
                </div>
            </div>
        </noscript>
        <div class="page">
            <div id="wrapper">
                <div id="loading" style="position: fixed;top: 50%;left: 50%;margin-top: -50px;"></div>
            </div>
            <div class="header-container">
                <div class="toplink-contaner">
                    <div class="container">
                        <div class="top-link-inner">
                            <div class="box-left">
                                <p class="welcome-msg"> </p>
                                <div class="block-header">
                                    <div class="phone"><label> </label>
                                        <p></p>
                                    </div>
                                    <div class="email"><label></label>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-right">
                                <div class="language-currency">
                                </div>
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/home')}}" title="Home">Home</a></div>
                                </div>
                                @else
                                @endif
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="top-link" id="top-link">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="select-lang"> Log out</i>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                    </div>
                                </div>
                                @else
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('login' )}}" title="Home">Log in</a></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header">
                    <div class="container">
                        <div class="header-content">
                            <div class="button">
                                <a href="#">
                                    <div class="button"><b></b></button>
                                </a>
                            </div>
                        </div>
                        <div class="quick-access">
                            <form id="search_mini_form" action="http://www.online-lighting-shop.com/catalogsearch/result/" method="get">
                                <div class="form-search">
                                    <label for="search">Search:</label>
                                    <input id="search" type="text" name="q" value="" class="input-text" maxlength="128" />
                                    <div class="loading_image_search"></div>
                                    <button type="submit" title="Search" class="button"><span><span>search</span></span></button>
                                    <div id="search_autocomplete" class="search-autocomplete"></div>
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    var searchForm = new Varien.searchForm('search_mini_form', 'search', 'Search');
                                    searchForm.initAutocomplete('catalogsearch/ajax/suggest/index.html', 'search_autocomplete');
                                    //]]>

                                    </script>
                                </div>
                            </form>
                            <div class="top-cart-wrapper">
                                <div class="top-cart-contain">
                                    <script type="text/javascript">
                                    $jq(document).ready(function() {
                                        var enable_module = $jq('#enable_module').val();
                                        if (enable_module == 0) return false;
                                    })

                                    </script>
                                    <div id="mini_cart_block">
                                        <div class="block-cart mini_cart_ajax">
                                            <div class="block-cart">
                                                <div class="top-cart-title">
                                                    <a href="{{ route('cart.index') }}">Shopping Cart
                                                        <span class="badge">{{ Cart::count()}}</span>
                                                    </a>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
       
    <div class="main-container col1-layout">
        <!-- Category Image-->
        <!--   -->
        <div class="container">
            <div class="breadcrumbs">
                <ul>
                    </li>
                </ul>
            </div>
            <div class="main">
                <div class="col-main">
                    
                   
                    <div class="product-view">
                        <div class="product-essential">
                            
                                <div class="row">
                                    <div class="col-sm-7 col-sms-12">
                                        <div class="product-img-box">
                                            <div class="more-views ma-more-img">
                                                <h2>More Views</h2>
                                                <ul>
                                                    <li>
                                                    </li>
                                                </ul>
                                              
                                                <!--##########-->
                                                <script type="text/javascript">
                                                //<![CDATA[
                                                $jq(document).ready(function() {
                                                    $jq('head').append('<style type="text/css"> .cloud-zoom-big { border:4px solid #cdcdcd }</style>');
                                                });
                                                //]]>

                                                </script>
                                            </div>
                                            <p class="product-image">
                                                <!-- images for lightbox -->
                                                <a href="{{asset('storage/' . $article->image)}}" class="ma-a-lighbox" rel="lightbox[rotation]"></a>
                                                <!--++++++++++++-->
                                                <a href="{{asset('storage/' . $article->image)}}" class="cloud-zoom" id="ma-zoom1" style="position: relative; display: block;" rel="adjustX:10, adjustY:-2, zoomWidth:450, zoomHeight:450">
                                                    <img src="{{asset('storage/' . $article->image)}}" alt="{{$article->name}}" title="{{$article->name}}" /> </a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="product-shop col-sm-5 col-sms-12">
                                        <div class="product-name">
                                            <h1>{{$article->name}}</h1>
                                        </div>
                                        {{$article->company}}
                                        {{-- <div class="levertijd">Delivery: <span>3-6 business days</span></div> --}}
                                        <div class="short-description">
                                            <!--<h2>Quick Overview</h2>-->
                                            <div class="std">
                                                {{$article->desc}}<br><br>
                                            </div>
                                        </div>
                                        <div class="box-container2">
                                            <div class="price-box">
                                                <p class="special-price">
                                                    <span class="price-label">Special Price</span>
                                                    <span class="price" id="product-price-23341">
                                                        €{{$article->price}} </span>
                                                </p>
                                              
                                            </div>
                                        </div>
                                        <div class="add-to-box-cart">
                                            <div class="add-to-cart">
                                                
                                              <div class="add-to-box-cart">
                                            
                                                {{-- <button type="button" class="btn btn-primary" onclick="productAddToCartForm.submit(this)"> <span><span>Add to Cart</span></span></button> --}}
                                                <a href=" {{route('cart.edit' ,$article->id)}}" title="Add to Cart" {{-- class="button btn-cart" --}} c class="badge badge-dark" style="padding: 10px;">Add To Cart </a>
                                            </div>
                                                    <div class="ma-desc">
                                                <a href="{{route('adminarticles.edit', $article)}}"><button class="btn btn-success">Edit</button></a>
                                     <form method="POST" action="{{route('adminarticles.destroy', $article)}}">
                                    @csrf
                                    @method('DELETE')
                                     <button type="submit" class="btn btn-danger">Remove</button></form>
                                    </div>
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="clearer"></div>
                                <div class="no-display">
                                    <input type="hidden" name="product" value="23341" />
                                    <input type="hidden" name="related_product" id="related-products-field" value="" />
                                </div>
                            
                           
                        </div>

</html>
