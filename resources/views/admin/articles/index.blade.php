
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<style>
    .warning {background-color: #ff9800;}
.warning:hover {background: #e68a00;}
div.gallery {
  margin: 5px;
  border: 1px solid #ccc;
  float: left;
  width: 180px;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
}
#my_centered_buttons { display: flex; justify-content: center; }
</style>
<!-- Mirrored from www.online-lighting-shop.com/wall-lamps by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 29 Mar 2020 22:12:36 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>

    </script>
    <title>Admin - Articles</title>
    <meta name="description" content="Buy your Wall lamps Online! Modern wall lamps, Classic wall lamps, LED wall lamps, Halogen wall lamps etc." />
    <meta name="keywords" content="lamps, lights, lamps online, lights online" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <link rel="icon" href="skin/frontend/assyrian/uk/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="skin/frontend/assyrian/uk/favicon.ico" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" /> 
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />
    
 
    <script type="text/javascript" src="../widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js"></script>
</head>

<body class=" catalog-category-view categorypath-wall-lamps category-wall-lamps">
    <!-- Google Tag Manager -->
    
        <div class="page">
            <div id="wrapper">
                <div id="loading" style="position: fixed;top: 50%;left: 50%;margin-top: -50px;"></div>
            </div>
            <div class="header-container">
                <div class="toplink-contaner">
                    <div class="container">
                        <div class="top-link-inner">
                            <div class="box-left">
                                <p class="welcome-msg"> </p>
                                <div class="block-header">
                                    <div class="phone"><label> </label>
                                        <p></p>
                                    </div>
                                    <div class="email"><label></label>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                               <div class="box-right">
                                <div class="language-currency">
                                </div>
                                
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/admin')}}" title="Home">Home</a></div>
                                </div>
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('adminarticles.create')}}" title="Home">Create an Article</a></div>
                                </div>
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('adminsubcategory.create')}}" title="Home">Create a Subcategory</a></div>
                                </div> 
                                @else
                                @endif

                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                      <div class="top-link" id="top-link">
                                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                           <i class="select-lang"> Log out</i>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                        </div>
                                </div>
                                @else 
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('login' )}}" title="Home">Log in</a></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header">
                    <div class="container">
                        <div class="header-content">
                            
                            <div class="button">
                                <a href="#">
                                    <div class="button"><b></b></button>
                                </a>
                            </div>
                        </div>
                        <div class="quick-access">
                            <form id="search_mini_form" action="{{route('adminsearch')}}" method="get">
                                <div class="form-search">
                                    <label for="search">Search:</label>
                                    <input id="search" type="text" name="search" value="" class="input-text" maxlength="128" />
                                    
                                    <button type="submit" title="Search" class="button"><span><span>search</span></span></button>
                                    
                                </div>
                            </form>
                            <div class="top-cart-wrapper">
                                <div class="top-cart-contain">
                                    <script type="text/javascript">
                                    $jq(document).ready(function() {
                                        var enable_module = $jq('#enable_module').val();
                                        if (enable_module == 0) return false;
                                    })

                                    </script>
                                    <div id="mini_cart_block">
                                        <div class="block-cart mini_cart_ajax">
                                            <div class="block-cart">
                                                <div class="top-cart-title">
                                                    <a href="{{ route('cart.index') }}">Shopping Cart
                                                        <span class="badge">{{ Cart::count()}}</span>
                                                    </a>
                                                </div>
                                                <div class="top-cart-content">
                                                    <p class="empty">You have no items in your shopping cart.</p>
                                                    <div class="top-subtotal">Subtotal: <span class="price">€0.00</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    </div>
    
    </div>
    </div>
    </div>
   
   
    <div class="main">
        <div class="row">
            
            </div>
            <div class="col-main col-xs-12 col-sm-9">
                <div style="display:none" class="amshopby-filters-top">
                    <div style="display:none" class="amshopby-overlay"></div>
                </div>
                <div id="tSPJUYBB">
                    <input type="text" name="kfjl9J" id="kfjl9J" value="d68ceb217553e1c6e365ac44d74ecc0e4940f5192e98cbde02043b25f85e9619" style="display:none !important;" />
                    <input type="hidden" name="0R9NVM" id="0R9NVM" value="" />
                    <input type="hidden" id="vgvvn3" name="vgvvn3" value="1585518408.2904" />
                </div>
                <script type="text/javascript">
                Element.addMethods({ getText: function(element) { a = $(element); return a.innerHTML; } });
                $(document).on('dom:loaded', function() { var b = "tSPJUYBB"; var c = $(b).getText(); if (c) { $('newsletter-validate-detail').insert({ top: c });
                        $(b).remove(); } });

                </script>
                <div class="page-title category-title">
                    <h1>Wall Lamps</h1>
                </div>
                <div class="toolbar">
                    <div class="sorter"> 
                    </div>
                    <div class="pager">
                        <div class="limiter hidden-xs">
                        </div>
                        <!--<p class="amount">
                                                        <strong></strong>
                    </p>-->
                        <div class="pages">
                            <strong>Page:</strong>
                            <ol>
                        </div>
                    </div>
                </div>
                <div class="product-grid-container">
                     @foreach($articles->chunk(3) as $articleChunk)
                    <ul class="products-grid row">
                      @csrf
                      @foreach($articleChunk as $article)
                        
                        <li class="ma-item_slider col-sm-4 col-sms-12 item ">
                            <div class="item-inner">
                                <div class="images-container">
                                    <a href="{{ route('adminarticles.show' , $article->id)}}" title="01-WL Eindhoven150 – Ace Design" class="product-image"><img src="{{asset('storage/' . $article->image)}}" alt="01-WL Eindhoven150 – Ace Design" max-height="1080" style="width: 560px;height: 300px;"></a>
                                  
                                </div>
                                <div class="des-container">
                                    <h2 class="product-name"><a href="ace-design-01-wl-eindhoven150-kubus.html" title="L&amp;W | Kubus">Emri: {{$article->name}}</a></h2>
                                    <ul class="add-to">
                                        <li>Kompania: {{$article->company}}</li>
                                    </ul>
                                    <div class="price-box">
                                        <span class="regular-price" id="product-price-17792">
                                            <span class="price">Qmimi: {{$article->price}}$</span>
                                        </span>
                                    </div>
                                            <div class="ma-desc">
                                <a href="{{route('adminarticles.edit', $article)}}"><button class="btn btn-success">Edit</button></a>
                                <form method="POST" action="{{route('adminarticles.destroy', $article)}}">
                                @csrf
                                 @method('DELETE')
                                     <button type="submit" class="btn btn-danger">Remove</button></form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                    @endforeach
                </div>
            </div>
            </li>
            <!--<p class="amount">
                                                        <strong></strong>
                    </p>-->
       

  {{-- <a href="{{route('adminarticles.create')}}">Create a product</a><br> --}}

{{-- @foreach($articles as $article)
  @csrf
<div class="polaroid">
  <img src="{{asset('storage/' . $article->image)}}"  style="width:100px">
  <div class="container">
  <h3><strong>{{$article->name}}</strong></h3>
  <p>{{$article->company}}</p>
  <h4><strong>${{$article->price}}</strong></h4>
  </div>
  <a href="{{route('adminarticles.show',$article->id)}}">View details</a><br>
</div> 

@endforeach--}}



















        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

</html>
