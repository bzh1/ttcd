<!DOCTYPE html>
<html>
<head>
	<title>Create a SubCategory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" /> 
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />
</head>
<body>
	<div class="toplink-contaner">
                    <div class="container">
                        <div class="top-link-inner">
                            <div class="box-left">
                                <p class="welcome-msg"> </p>
                                <div class="block-header">
                                    <div class="phone"><label> </label>
                                        <p></p>
                                    </div>
                                    <div class="email"><label></label>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                               <div class="box-right">
                                <div class="language-currency">
                                </div>
                                
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/admin')}}" title="Home">Home</a></div>
                                </div>
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('adminarticles.index')}}" title="Home">Articles</a></div>
                                </div> 
                                @else
                                @endif

                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                      <div class="top-link" id="top-link">
                                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                           <i class="select-lang"> Log out</i>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form></a>
                                        </div>
                                </div>
                                @else 
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('login' )}}" title="Home">Log in</a></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
<div class="container">
	<h1 class="w3-container w3-blue">Create a SubCategory </h1>
	<form action="{{route('adminsubcategory.store')}}" method="POST" enctype="multipart/form-data">
		@csrf
	<div class="form-group">
    	<label for="name">Subcategory Name</label>
    	<input type="text" class="form-control" name="name" placeholder="Name" value="{{old('name')}}">
        @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
  	</div>
    <div class="form-group">
        <label for="name">Category</label>
        <select id="multiple-checkboxes" multiple="multiple" name="category_id[]">
            @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
        @error('category_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="image">Image</label>
        <input type="file" class="form-control" name="image" placeholder="image" value="{{old('image')}}">
        @error('image')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
    </div>
		<button class="btn btn-danger" style="float: left"  type="submit">Add</button>
        <div style="text-align: right;"><a href="{{ route('adminarticles.create') }}" class="btn btn-success">Create an Article?</a></div>
	</form>
</div>	 

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
<script type="text/javascript">
     $(document).ready(function() {
        $('#multiple-checkboxes').multiselect({
          includeSelectAllOption: true,
        });
    });
</script>
</body>
</html>