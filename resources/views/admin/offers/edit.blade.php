
<!DOCTYPE html>
<html>
<head>
	<title>Ndrysho Oferten</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
	body{
    background: -webkit-linear-gradient(left, #0072ff, #00c6ff);
}
.contact-form{
    background: #fff;
    margin-top: 10%;
    margin-bottom: 5%;
    width: 70%;
}
.contact-form .form-control{
    border-radius:1rem;
}
.contact-image{
    text-align: center;
}
.contact-image img{
    border-radius: 6rem;
    width: 11%;
    margin-top: -3%;
    transform: rotate(29deg);
}
.contact-form form{
    padding: 14%;
}
.contact-form form .row{
    margin-bottom: -7%;
}
.contact-form h3{
    margin-bottom: 8%;
    margin-top: -10%;
    text-align: center;
    color: #0062cc;
}
.contact-form .btnContact {
    width: 50%;
    border: none;
    border-radius: 1rem;
    padding: 1.5%;
    background: #dc3545;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
}
.btnContactSubmit
{
    width: 50%;
    border-radius: 1rem;
    padding: 1.5%;
    color: #fff;
    background-color: #0062cc;
    border: none;
    cursor: pointer;
}
</style>
</head>
<body>
<div class="container contact-form">
            <div class="contact-image">
                <img src="{{asset('img/offer.jpeg')}}" alt="foto"/>
            </div>
            <form method="POST" action="{{ route('adminoffers.update' , $offer->id) }}">
                @csrf
                @method('PATCH')
                <h3>Ndrysho Oferten Per Klientin E Caktuar</h3>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Emri i ofertes" value="{{$offer->name}}">
                              @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="user_id" id="klienti" value="{{ old('user_id')}}">
                        <option disabled value>Select User...</option>
                            @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                    </select>
                      @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" placeholder="Emaili i klientit *" value="{{$offer->email}}">
                             @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <a style="float: left; " class="btn btn-danger" href="{{route('adminoffers.index')}}"> Anulo</a>
                    </div>
                    <div class="col-md-6">
                    {{--     <div class="form-group">
                            <select class="form-control form-control-sm" name="article_id" id="produkti" value="{{$offer->article_id}}">
                                    <option disabled value>Select Article...</option>
                                    @foreach($articles as $article)
                                        <option value="{{ $article->id }}">{{ $article->name }} - Kategoria > {{ $article->category['name'] }}</option>
                                    @endforeach
                                </select>
                                  @error('article_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div> --}}
                            <div class="form-group">
                                    <input type="number" name="qty" class="form-control" placeholder="Sasia e produkteve" value="{{$offer->qty}}">
                                      @error('qty')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                        
                          <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon">Eur</div>
                                    <input  min="0" step="1.00" name="price"  id="exampleInputAmount" class="form-control" placeholder="Vendos Qmimin Total" value="{{$offer->price}}">
                            </div>
                                      @error('price')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                          </div>
                        
                        <div class="form-group">
                            <input style="background-color: green;" style="text-align: center;" type="submit" name="btnSubmit" class="btnContact" value="Perfundo" / >
                        </div>
                    </div>
                </div>
            </form>
</div>
</body>
</html>


<!------ Include the above in your HEAD tag ---------->

