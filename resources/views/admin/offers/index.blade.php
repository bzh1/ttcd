<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="{{asset('css\scrolling-nav.css')}}" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" /> 
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />
    <style type="text/css">


#about {
	padding: 30px;
}

    * {
        box-sizing: border-box;
    }

    /* Create two equal columns that floats next to each other */
    .column {
        float: left;
        width: 50%;
        padding: 10px;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    /* Style the buttons */
    .btn {
        border: none;
        outline: none;
        padding: 12px 16px;
        background-color: #f1f1f1;
        cursor: pointer;
    }

    .btn:hover {
        background-color: #ddd;
    }

    .btn.active {
        background-color: #666;
        color: white;
    }

    .button {

        border: none;
        padding: 16px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
        background-color: white;
        color: black;
        border: 2px solid #008CBA;
        text-decoration: none;
    }

    .button:hover {
        background-color: #008CBA;
        color: white;
        text-decoration: none;
    }

    #linki {
        text-decoration: none;
        color: white;
        font-size: 20px;
    }

    #linki:hover {
        color: black;
    }

   
    </style>
    <title>Admin -  Ofertat</title>
    
</head>

<body id="page-top">

    <div class="toplink-contaner">
                    <div class="container">
                        <div class="top-link-inner">
                            <div class="box-left">
                                <p class="welcome-msg"> </p>
                                <div class="block-header">
                                    <div class="phone"><label> </label>
                                        <p>2233665588</p>
                                    </div>
                                    <div class="email"><label></label>
                                        <p>emailiadminit@gmail.com</p>
                                    </div>
                                </div>
                            </div>
                               <div class="box-right">
                                <div class="language-currency">
                                </div>
                                
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/admin')}}" title="Home">Home</a></div>
                                </div>
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('adminarticles.index')}}" title="Home">Articles</a></div>
                                </div> 
                                @else
                                @endif

                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                      <div class="top-link" id="top-link">
                                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                           <i class="select-lang"> Log out</i>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form></a>
                                        </div>
                                </div>
                                @else 
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('login' )}}" title="Home">Log in</a></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">

        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#"><h1 style="font-size: 32px;">Oferta</h1></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" style="opacity: 0;">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        
                        <a class="navbar-brand js-scroll-trigger" style="font-size: 20px; margin-left: 20px;" href="{{ url('/') }}">Shopi</a>
                    </li>
                    <li class="nav-item">
                        <a style=" font-size :20px;" class="navbar-brand js-scroll-trigger" href="#">Ofertat</a>
                    </li>
                    <li class="nav-item">
                         
                                    
                    </li>
                                    </ul>
                                    <div class="block-cart mini_cart_ajax" style="margin-bottom: 15px; float: right ; font-size: 16px;">
                                            <div class="block-cart">
                                                <div class="top-cart-title">
                                                    <a href="{{ route('cart.index') }}">
                                                        <span class="badge">{{ Cart::count()}}</span>
                                                    </a>
                                                </div>
                                                <div class="top-cart-content">
                                                    <p class="empty">You have no items in your shopping cart.</p>
                                                    <div class="top-subtotal">Subtotal: <span class="price">€0.00</span></div>
                                                </div>
                                            </div>
                                        </div>
            </div>

        </div>
    </nav>
    <header class="bg-primary text-white">
        <div class="container text-center">
            <h1>Ketu do ti krijosh ofertat</h1>
            <p class="lead"><a id="linki" href="{{ route('adminoffers.create') }}"><strong>KLIKO KETU</strong></a> ose ne linkun ne navbar per te krijuar oferta</p>
        </div>
    </header>
    <section id="about">
        <h2 style="font-size: 32px;">Ketu i ke te listuara ofertat</h2>
        <p>Per ta ndryshuar menyren e pamjes se ofertave klikoni ne keto butona</p>
        <div id="btnContainer"> 
            <button class="btn" onclick="listView()"><i class="fa fa-bars"></i> List</button>
            <button class="btn active" onclick="gridView()"><i class="fa fa-th-large"></i> Grid</button>
        </div>
        <br>
        @foreach($offers->chunk(2) as $offerChunk)
        <div class="row">
            @foreach($offerChunk as $offer)
            <div class="column" style="background-color:#F8F9FA;">

                 <h2>{{ $offer->name}} </h2>
                @foreach($offer->article as $article)
               
                    
                <img src="{{asset('storage/' . $article->image)}}" style="width: 400px; height: 281px;">
                <h3>Emri i artikullit: {{ $article->name}} </h3>
                <h6> Qmimi i meparshem: {{$article->price}} 
                    @endforeach
                    {{-- {{dd($article->name)}} --}}
                <h6>Cmimi i ofertes: {{ $offer->price}} </h6>
                
             
                <h6>User E-mail: {{ $offer->email}} </h6>
                <h6>User Name: {{ $offer->user->name}} </h6>
                <h6>Sasia: {{ $offer->qty}} </h6>
                 {{-- @endforeach --}}
             
                <form action="{{ route('adminoffers.destroy' , $offer)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="{{ route('adminoffers.edit' , $offer->id)}}" style=" margin: 5px ; padding: 10px; border-radius: 8px; "  class=" btn-success" type="submit"> Ndrysho Oferten</a>
                    <button style=" margin: 5px ; padding: 10px; border-radius: 8px; " class=" btn-danger" type="submit"> Fshi Oferten</button>
                </form>
                
                <hr>
            </div>
            @endforeach
        </div>
        @endforeach
       
    </section>
    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Foteri I Websajtit</p>
        </div>
        <!-- /.container -->
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom JavaScript for this theme -->
    <script src="js/scrolling-nav.js"></script>
    <script>
    // Get the elements with class="column"
    var elements = document.getElementsByClassName("column");

    // Declare a loop variable
    var i;

    // List View
    function listView() {
        for (i = 0; i < elements.length; i++) {
            elements[i].style.width = "100%";
        }
    }

    // Grid View
    function gridView() {
        for (i = 0; i < elements.length; i++) {
            elements[i].style.width = "50%";
        }
    }

    /* Optional: Add active class to the current button (highlight it) */
    var container = document.getElementById("btnContainer");
    var btns = container.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
    }
    </script>
</body>

</html>