
<!DOCTYPE html>
<html>
<head>
	<title>Krijo Oferten</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <style type="text/css">
	body{
    background: -webkit-linear-gradient(left, #0072ff, #00c6ff);
}
.contact-form{
    background: #fff;
    margin-top: 10%;
    margin-bottom: 5%;
    width: 70%;
}
.contact-form .form-control{
    border-radius:1rem;
}
.contact-image{
    text-align: center;
}
.contact-image img{
    border-radius: 6rem;
    width: 11%;
    margin-top: -3%;
    transform: rotate(29deg);
}
.contact-form form{
    padding: 14%;
}
.contact-form form .row{
    margin-bottom: -7%;
}
.contact-form h3{
    margin-bottom: 8%;
    margin-top: -10%;
    text-align: center;
    color: #0062cc;
}
.contact-form .btnContact {
    width: 50%;
    border: none;
    border-radius: 1rem;
    padding: 1.5%;
    background: #dc3545;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
}
.btnContactSubmit
{
    width: 50%;
    border-radius: 1rem;
    padding: 1.5%;
    color: #fff;
    background-color: #0062cc;
    border: none;
    cursor: pointer;
}
</style>
</head>
<body>
<div class="container contact-form">
            <div class="contact-image">
                <img src="{{asset('img/offer.jpeg')}}" alt="foto"/>
            </div>
            <form method="POST" action="{{ route('adminoffers.store') }}">
                @csrf
                <h3>Krijo Oferten Per Klientin E Caktuar</h3>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Emri i ofertes" value="{{ old('name')}}">
                              @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="user_id" id="klienti" value="{{ old('user_id')}}">
                        <option disabled selected value>Select User...</option>
                            @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                    </select>
                      @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" placeholder="Emaili i klientit *" value="{{ old('email')}}">
                             @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                            <a style="float: left; " class="btn btn-danger" href="{{route('adminoffers.index')}}"> Anulo</a>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="article">Artikulli: </label>
                            <select multiple class=""id="multiple-checkboxes" name="article_id[]" value="{{ serialize('article_id')}}">
                                    @foreach($articles as $article)
                                        <option value="{{ $article->id }}">{{ $article->name }} - Qmimi : {{ $article->price }}$</option>
                                    @endforeach
                                </select>
                                  @error('article_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                            <div class="form-group">
                                    <input type="number" min="1" name="qty" class="form-control" placeholder="Sasia e produkteve" value="{{ old('qty')}}">
                                      @error('qty')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                        
                          <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon">%</div>
                                    <input  min="1" step="1.00" name="price"  id="exampleInputAmount" class="form-control" placeholder="Vendos Zbritjen pa perqindje" value="{{ old('price')}}">
                            </div>
                                      @error('price')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                          </div>
                        <div class="form-group">
                            <input style="text-align: center; background-color: green;" type="submit" name="btnSubmit" class="btnContact" value="Perfundo" / >
                        </div> 
                        
                        
                    </div>
                </div>
            </form>
</div>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
<script type="text/javascript">
     $(document).ready(function() {
        $('#multiple-checkboxes').multiselect({
          includeSelectAllOption: true,
        });
    });
</script>
</body>
</html>


<!------ Include the above in your HEAD tag ---------->

