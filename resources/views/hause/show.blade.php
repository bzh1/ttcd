<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Mirrored from www.online-lighting-shop.com/eglo-49394-charterhouse by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 29 Mar 2020 22:35:39 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>{{$article->name}}</title>
    <meta name="description" content="49394 from Eglo. Order quickly and reliably at the biggest online lighting store.
" />
    <meta name="keywords" content="49394,Eglo,, verlichting" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <link rel="icon" href="skin/frontend/assyrian/uk/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="skin/frontend/assyrian/uk/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />

    <script type="text/javascript">

    //<![CDATA[
    Mage.Cookies.path = 'index.html';
    Mage.Cookies.domain = '.www.online-lighting-shop.com';
    //]]>

    </script>
    <script type="text/javascript">
    //<![CDATA[
    optionalZipCountries = ["HK", "IE", "MO", "PA"];
    //]]>

    </script>
    <!-- BEGIN GOOGLE UNIVERSAL ANALYTICS CODE -->
    <script type="text/javascript">
    //<![CDATA[
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');


    ga('create', 'UA-74104491-1', 'auto');

    ga('send', 'pageview');

    //]]>

    </script>
    <!-- END GOOGLE UNIVERSAL ANALYTICS CODE -->
    <script type="text/javascript">
    (function(w, d, s, r, n) {
        w.TrustpilotObject = n;
        w[n] = w[n] || function() {
            (w[n].q = w[n].q || []).push(arguments) };
        a = d.createElement(s);
        a.async = 1;
        a.src = r;
        a.type = 'text/java' + s;
        f = d.getElementsByTagName(s)[0];
        f.parentNode.insertBefore(a, f)
    })(window, document, 'script', '../invitejs.trustpilot.com/tp.min.js', 'tp');
    tp('register', '');

    </script>
    <script type="text/javascript">
    //<![CDATA[
    var Translator = new Translate([]);
    //]]>

    </script>
    <meta name="google-site-verification" content="v8jZ9kdUj-R1kDZBqPlIFyBy9LYFcZKBQ2pzvJuNepI" />
    <script type="text/javascript">
    function rankingsPush() {
        var url = String(document.referrer);
        if (url.indexOf("google.nl") != -1) {
            var urlVars = {};
            var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
                urlVars[key] = value;
            });
            _gaq.push(['_setCustomVar', '1', 'Rankings', urlVars["cd"], 1]);
        }
    }

    </script>
    <script type="text/javascript">
    decorateTable('product-attribute-specs-table')

    </script>
    <script type="text/javascript">
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block") {
            ele.style.display = "none";
            text.innerHTML = "Meer info >";
        } else {
            ele.style.display = "block";
            text.innerHTML = "Verberg meer info >";
        }
    }

    </script>
    <script type="text/javascript" src="../widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js"></script>
</head>

<body class=" catalog-product-view catalog-product-view product-eglo-49394-charterhouse">
    <!-- Google Tag Manager -->
    <script type="text/javascript">
    dataLayer = [{
        "pageCategory": "product-detail"
    }];

    </script>
    <script type="text/javascript">
    dataLayer.push({
        "ecommerce": {
            "detail": {
                "actionField": {
                    "list": "Catalog"
                },
                "products": {
                    "name": "{{$article->name}}",
                    "id": "49394",
                    "price": "62.95",
                    "category": "Wall Lamps"
                }
            }
        }
    });

    </script>
    <script type="text/javascript">
    function gtmDataBuilder(gtmData) { dataLayer.push(gtmData); return true; }

    </script> <noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-MG56X5" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MG56X5');</script>
    <!-- End Google Tag Manager -->
    <div class="wrapper">
        <noscript>
            <div class="global-site-notice noscript">
                <div class="notice-inner">
                    <p>
                        <strong>JavaScript seems to be disabled in your browser.</strong><br />
                        You must have JavaScript enabled in your browser to utilize the functionality of this website. </p>
                </div>
            </div>
        </noscript>
        <div class="page">
            <div id="wrapper">
                <div id="loading" style="position: fixed;top: 50%;left: 50%;margin-top: -50px;"></div>
            </div>
            <div class="header-container">
                <div class="toplink-contaner">
                    <div class="container">
                        <div class="top-link-inner">
                            <div class="box-left">
                                <p class="welcome-msg"> </p>
                                <div class="block-header">
                                    <div class="phone"><label> </label>
                                        <p></p>
                                    </div>
                                    <div class="email"><label></label>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-right">
                                <div class="language-currency">
                                </div>
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url()->previous()}}" title="Home">Shopi</a></div>
                                </div>
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/home')}}" title="Home">Home</a></div>
                                </div>
                                @else
                                @endif
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="top-link" id="top-link">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="select-lang"> Log out</i>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                    </div>
                                </div>
                                @else
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('login' )}}" title="Home">Log in</a></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header">
                    <div class="container">
                        <div class="header-content">
                            <div class="button">
                                <a href="#">
                                    <div class="button"><b></b></button>
                                </a>
                            </div>
                        </div>
                        <div class="quick-access">
                           
                           <form class="form-inline my-2 my-lg-0"  style=" float: right;" method="GET" role="search" action="{{route('search.create')}}">
                                @csrf
                                <input class="form-control mr-sm-2" placeholder="Search" name="search" aria-label="Search" style="margin-top: 5px;">
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" style="margin-top: 5px;">Search</button>
                              </form> 
                            </form>
                            </form>
                            <div class="top-cart-wrapper">
                                <div class="top-cart-contain">
                                    <script type="text/javascript">
                                    $jq(document).ready(function() {
                                        var enable_module = $jq('#enable_module').val();
                                        if (enable_module == 0) return false;
                                    })

                                    </script>
                                    <div id="mini_cart_block">
                                        <div class="block-cart mini_cart_ajax">
                                            <div class="block-cart">
                                                <div class="top-cart-title">
                                                    <a href="{{ route('cart.index') }}">Shopping Cart
                                                        <span style="font-size: 13px;" class="badge">{{ Cart::count()}}</span>
                                                    </a>
                                                </div>
                                                <div class="top-cart-content">
                                                    <p class="empty">You have no items in your shopping cart.</p>
                                                    <div class="top-subtotal">Subtotal: <span class="price">€0.00</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="ma-nav-mobile-container visible-xs">
                <div class="navbar">
                    <div id="navbar-inner" class="navbar-inner navbar-inactive">
                        <div class="menu-mobile">
                            <a class="btn btn-navbar navbar-toggle">
                                <span class="icon-bar">icon1</span>
                                <span class="icon-bar">icon2</span>
                                <span class="icon-bar">icon3</span>
                            </a>
                            <span class="brand navbar-brand">Categories</span>
                        </div>
                        <ul id="ma-mobilemenu" class="mobilemenu nav-collapse collapse">
                            <li class="level0 nav-1 level-top first">
                                <a href="pendant-lamps.html" class="level-top">
                                    <span>Pendant Lamps</span>
                                </a>
                            </li>
                            <li class="level0 nav-2 level-top">
                                <a href="floor-lamps.html" class="level-top">
                                    <span>Floor Lamps</span>
                                </a>
                            </li>
                            <li class="level0 nav-3 level-top">
                                <a href="wall-lamps.html" class="level-top">
                                    <span>Wall Lamps</span>
                                </a>
                            </li>
                            <li class="level0 nav-4 level-top">
                                <a href="table-lamps.html" class="level-top">
                                    <span>Table Lamps</span>
                                </a>
                            </li>
                            <li class="level0 nav-5 level-top">
                                <a href="ceiling-lights.html" class="level-top">
                                    <span>Ceiling Lights</span>
                                </a>
                            </li>
                            <li class="level0 nav-6 level-top">
                                <a href="bathroom-lighting.html" class="level-top">
                                    <span>Bathroom Lighting</span>
                                </a>
                            </li>
                            <li class="level0 nav-7 level-top">
                                <a href="spotlights.html" class="level-top">
                                    <span>Spotlights</span>
                                </a>
                            </li>
                            <li class="level0 nav-8 level-top">
                                <a href="recessed-spots.html" class="level-top">
                                    <span>Recessed Spots</span>
                                </a>
                            </li>
                            <li class="level0 nav-9 level-top">
                                <a href="children-lighting.html" class="level-top">
                                    <span>Children's lighting</span>
                                </a>
                            </li>
                            <li class="level0 nav-10 level-top last">
                                <a href="outdoor-lighting.html" class="level-top">
                                    <span>Outdoor Lighting</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="nav-container visible-lg visible-md">
                <div id="pt_custommenu" class="pt_custommenu">
                    <div id="pt_menu13" class="pt_menu nav-1 pt_menu_no_child">
                    </div>
                </div>
                <div class="clearBoth"></div>
            </div>
        </div>
        <script type="text/javascript">
        //<![CDATA[
        var CUSTOMMENU_POPUP_EFFECT = 0;
        var CUSTOMMENU_POPUP_TOP_OFFSET = 56;
        //]]>

        </script>
    </div>
    <script type="text/javascript">
    $jq(window).scroll(function() {
        if ($jq(this).scrollTop() > 80) {
            $jq('.header').addClass("fix-header");
        } else {
            $jq('.header').removeClass("fix-header");
        }
    });

    </script>
    <div class="main-container col1-layout">
        <!-- Category Image-->
        <!--   -->
        <div class="container">
            <div class="breadcrumbs">
                <ul>
                    </li>
                </ul>
            </div>
            <div class="main">
                <div class="col-main">
                    <div id="tSPJUYBB">
                        <input type="text" name="kfjl9J" id="kfjl9J" value="d68ceb217553e1c6e365ac44d74ecc0e4940f5192e98cbde02043b25f85e9619" style="display:none !important;" />
                        <input type="hidden" name="0R9NVM" id="0R9NVM" value="" />
                        <input type="hidden" id="vgvvn3" name="vgvvn3" value="1585518460.1228" />
                    </div>
                    <script type="text/javascript">
                    Element.addMethods({ getText: function(element) { a = $(element); return a.innerHTML; } });
                    $(document).on('dom:loaded', function() { var b = "tSPJUYBB"; var c = $(b).getText(); if (c) { $('newsletter-validate-detail').insert({ top: c });
                            $(b).remove(); } });

                    </script>
                    <script type="text/javascript">
                    var optionsPrice = new Product.OptionsPrice([]);

                    </script>
                    <div class="product-view">
                        <div class="product-essential">
                            
                                <div class="row">
                                    <div class="col-sm-7 col-sms-12">
                                        <div class="product-img-box">
                                            <div class="more-views ma-more-img">
                                                <h2>More Views</h2>
                                                <ul>
                                                    <li>
                                                    </li>
                                                </ul>
                                                <!-- thumbnail for lighbox-->
                                                <ul class="mt-thumb-light" style="display:none;">
                                                    <li>
                                                        <a href="   <!--foto anash-->" rel="lightbox[rotation]" title="Eglo Charterhouse"></a>
                                                    </li>
                                                    <li>
                                                        <a href="<!--foto anash-->" rel="lightbox[rotation]" title="Eglo Charterhouse"></a>
                                                    </li>
                                                    <li>
                                                        <a href="<!--foto anash-->" rel="lightbox[rotation]" title="Eglo Charterhouse"></a>
                                                    </li>
                                                </ul>
                                                <!--##########-->
                                                <script type="text/javascript">
                                                //<![CDATA[
                                                $jq(document).ready(function() {
                                                    $jq('head').append('<style type="text/css"> .cloud-zoom-big { border:4px solid #cdcdcd }</style>');
                                                });
                                                //]]>

                                                </script>
                                            </div>
                                            <p class="product-image">
                                                <!-- images for lightbox -->
                                                <a href="{{asset('storage/' . $article->image)}}" class="ma-a-lighbox" rel="lightbox[rotation]"></a>
                                                <!--++++++++++++-->
                                                <a href="{{asset('storage/' . $article->image)}}" class="cloud-zoom" id="ma-zoom1" style="position: relative; display: block;" rel="adjustX:10, adjustY:-2, zoomWidth:450, zoomHeight:450">
                                                    <img src="{{asset('storage/' . $article->image)}}" alt="{{$article->name}}" title="{{$article->name}}" /> </a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="product-shop col-sm-5 col-sms-12">
                                        <div class="product-name">
                                            <h1>{{$article->name}}</h1>
                                        </div>
                                        {{$article->company}}
                                        {{-- <div class="levertijd">Delivery: <span>3-6 business days</span></div> --}}
                                        <div class="short-description">
                                            <!--<h2>Quick Overview</h2>-->
                                            <div class="std">
                                                {{$article->desc}}<br><br>
                                                <!--    Article 49394 from the series Charterhouse has the following specifications:<br><br>-->
                                                {{-- You will find all specifications of the Eglo lamp below. <br><br> --}}
                                                {{-- Our consultants will be happy to help you if you have any questions about Eglo Charterhouse. --}}
                                                <!--Deze mooi ontworpen  Wandlamp uit het assortiment van Eglo bestelt u snel en betrouwbaar bij de grootste Online Lampen Winkel van Nederland! 
                                <br/><br/>
                            Hieronder vind u de volledige specificatie van de Charterhouse Wandlamp van Eglo. Voor vragen kunt u zich uiteraard ook wenden tot onze <a href="http://www.online-lampen-winkel.nl/showroom/">helpdesk</a> of <a href="http://www.online-lampen-winkel.nl/showroom/">showroom</a>. -->
                                                </div>
                                        </div>
                                        <div class="box-container2">
                                            <div class="price-box">
                                                <p class="special-price">
                                                    <span class="price-label">Special Price</span>
                                                    <span class="price" id="product-price-23341">
                                                        €{{$article->price}} </span>
                                                </p>
                                                {{-- <p class="old-price">
                                                    <span class="price-label">Regular Price:</span>
                                                    <span class="price" id="old-price-23341">
                                                        €62.95 </span>
                                                </p> --}}
                                            </div>
                                        </div>
                            

                                    <div class="add-to-cart">
                                            <label for="qty">Quantity:</label>
                                            <form action="{{route('cart.edit' ,$article->id)}}" method="GET">
                                                @csrf
                                    <div class="qty-button">
                                        
                                        <input type="text" name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty">
                                        <div class="button-icon">
                                            <div class="box-icon button-minus">
                                                <input type="button" onclick="var qty_el = document.getElementById('qty'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;" class="qty-increase ">
                                            </div>
                                            <div class="box-icon button-plus">
                                                <input type="button" onclick="var qty_el = document.getElementById('qty'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty > 0 ) qty_el.value--;return false;" class="qty-decrease">
                                            </div>
                                            
                                            
                                        </div>
                                        
                                    </div>      
                                     
                                    <button type="submit" class="button btn-cart"> <span><span>Add to Cart</span></span></button>
                                    </form>
                                    </div>
                                        <div class="add-to-box">
                                            @if($article->isFavorited() == true)
                                            <br>
                                            <ul class="add-to-links">
                                               <a style="text-decoration: none;  "class="link-wishlist" href="{{route('favorite.show' ,$article->id)}}" ><h1>Remove from wishlist</h1></a> 
                                            </ul>
                                            @else
                                            <ul class="add-to-links">
                                               <a style="text-decoration: none; " class="link-wishlist" href="{{route('favorite.edit' ,$article->id)}}" ><h1>Add to wishlist</h1></a> 
                                            </ul>
                                            @endif
                                            {{-- <p class="email-friend" title="Email"><a href="sendfriend/product/send/id/23341/index.html">Email to a Friend</a></p> --}}
                                        </div>
                                            <hr>
                                            </div>
                                       



      














                                        <div class="product-social">
                                            <!-- AddThis Button BEGIN -->
                                            <div class="addthis_toolbox addthis_default_style ">
                                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                                <a class="addthis_button_tweet"></a>
                                                <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                                                <a class="addthis_counter addthis_pill_style"></a>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="clearer"></div>
                                <div class="no-display">
                                    <input type="hidden" name="product" value="23341" />
                                    <input type="hidden" name="related_product" id="related-products-field" value="" />
                                </div>
                           
                          
                        </div>

</html>
