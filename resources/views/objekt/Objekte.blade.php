 <!DOCTYPE html>

<style>
.headero {
  padding: 20px;
  text-align: center;
  background: grey;
  color: white;
}
.footer {
  padding: 20px;
  text-align: center;
  background: grey;
  }

    </style>
        <html lang="de-CH" class="no-js">
 
    <head>
        <title>OBJECTIN</title>
		<link rel="stylesheet" href="{{asset('css\app.css')}}">
        <link rel="stylesheet" href="{{asset('css\icons.css')}}">
       <meta name="robots" content="all,noodp">
          <meta name="viewport" content="width=device-width, initial-scale=1">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="application-name" content="Elektrogrosshandel">
            <meta name="theme-color" content="#0083C3">
            <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="csrf-token" content="k8E543y1c8pfewXIGW0kjeSwVS8ejVvAU6aiuCRSCiic0uXQcyvO8sXUDcXYhrAC">
	        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/static/images/favicon/apple-touch-icon-57x57.png">
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/images/favicon/apple-touch-icon-114x114.png">
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/images/favicon/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/images/favicon/apple-touch-icon-144x144.png">
            <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/static/images/favicon/apple-touch-icon-60x60.png">
            <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/images/favicon/apple-touch-icon-120x120.png">
            <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/images/favicon/apple-touch-icon-76x76.png">
            <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/images/favicon/apple-touch-icon-152x152.png">
            <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/favicon.ico">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-196x196.png" sizes="196x196">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-96x96.png" sizes="96x96">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-32x32.png" sizes="32x32">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-16x16.png" sizes="16x16">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-128.png" sizes="128x128">   
    <meta name="msapplication-config" content="/static/ieconfig.xml">
    <link rel="manifest" href="/static/manifest.webmanifest"> 
            <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=greek">    
            <link rel="stylesheet" type="text/css" href="/static/css/icons.css?f4a2080209ffae2cdba0e">
            <link rel="stylesheet" type="text/css" href="/static/css/app.css?f4a2080209ffae2cdba0e">      
            <script>              
                var cssLoaded = true;
            </script>
            <script defer src="/static/js/app.js?f4a2080209ffae2cdba0e"></script>
            <link rel="canonical" href="https://www.elektrogrosshandel.ch/de/costcenters/">               
                        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53269130-1"></script>                
                    <script>
                        window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                        gtag('config', 'UA-53269130-1');
                        
                    </script> 
    </head>
	   

<header class="shopheader">
<section class="shopheader__middle">             
        </a>
    </div>

                    
                        <div class="shopheader__search">
                            
                            <div class="shopheader__search-instant-results" data-instant-search-results></div>
                        </div>
                    
                    
                        
                     <section class="shopheader__account"> 
                     <a href="{{ url('/')}}"><i class="icon icon-cart medium"></i>Shopi</a>                   
                                    <a href="{{ url('/home') }}" data-toggle="account-menu" title="Mein Konto">

                                        <i class="icon icon-user medium"></i>Dashbard
                                    </a>
                                    



                                    
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                                                         Log Out
                                                            
                                                            <i class="fa fa-sign-out" style="font-size:20px;color:red"></i>
                                                        

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                    




                                      
                                    
                                    <div class="shopheader-menu shopheader-menu--account dropdown-pane widget"
                                         id="account-menu"
                                         data-dropdown
                                         data-hover="true"
                                         data-hover-pane="true"
                                         data-position="bottom"
                                         data-alignment="right">

                                        <div class="widget__heading">
                     <form method="POST" class="width-100" action="/de/account/logout/">
                                                    <input type="hidden" name="csrfmiddlewaretoken" value="QAtYp5OejKarMVrbXLYOQpQwNCxTCyugqyZbPE75JU3oyTrjtntiFDVUvWmDA4zi">
                                                    <button type="submit" class="button hollow margin-top-1 width-100">
                                                        <i class="icon icon-logout medium"></i>
                                                        <span>Abmelden</span>
                                                    </button>
                                                </form>
                                                
                                            
                                        </div>
                                    </div>
                                
                            
                            
                              
                                   <a
                                    href="{{ route('cart.index') }}"
                                    title=" "
                                    data-toggle="cart-menu"
                                    data-bind-prop-set="cart:activeCartLabel,title=Warenkorb {}">
                                
                                    <i class="icon icon-cart medium"></i>
                                    <a href="html.html">
                                    <span class="badge hide" data-bind-text="cart:activePositionCount" data-bind-class="cart:isActiveEmpty,hide">
                                      
                                    </span>
                                </a>
                                <div
                                    class="shopheader-menu shopheader-menu--cart dropdown-pane widget "
                                    
                                    data-bind-prop-set="cart:hasActivePositions,data-cart-preview=/de/cart/preview"
                                    data-cart-preview=/de/cart/preview
                                    id="cart-menu"
                                    data-dropdown
                                    data-hover="true"
                                    data-hover-pane="true"
                                    data-position="bottom"
                                    data-alignment="right"
                                >
                                    <div class="cart-preview" data-cart-preview-widget>
                                        <div class="cart-preview__loader"><i class="icon loader"></i></div>
                                        <div
                                            class="cart-preview__content"
                                            data-bind-innerhtml="cart:preview"
                                        ></div>
                                    </div>
                                </div>
                            
                        </section>
                    
                </div>
            </section>
                                            </div>
                                                
                                            </div>
                                        </div>
            
                </div>
            </nav>
        
    </header> 

        <header class="print-header grid-x">
    
        <div class="cell small-5">
            <img src="/static/images/logo.svg" class="print-header__logo">
        </div>
    
    
        <div class="cell small-7">
            <div class="grid-x grid-margin-x">
                <div class="cell auto">
                  
                </div>
                   </div>
            </div>
        </div>
    
</header>


    

    
        <main id="app" class="bg-white">
            
    
    
    <header
        class="shopsubheader shopsubheader--small shopsubheader--buttons"
        style=""
    >
        <div class="container">          
                <h1 style ="text-align:Center;">
    
    Objekte
</h1>
        <a href="{{route('objekt.create')}}" class="button white hollow small">
            <i class="icon icon-plus"></i>
            <span>KRIJOJ OBJEKTIN</span>
        </a>
        <a href="" class="button white hollow small">
            <i class="icon icon-plus"></i>
            <span>EDITOR</span>
        </a>
        </div>
    </header>
    <section class="container medium padding-vertical-3">
        <form class="input-group inline-button"  style=" float: right;" method="GET" role="search" action="{{route('search.create')}}">
                    @csrf
                    <input class="input-group-field" placeholder="Search" name="search" aria-label="Search" style="margin-top: 5px;">
                    <button class="button clear" type="submit" style="margin-top: 5px;">Search</button>
                  </form> 
                 
        {{-- <table class="rows width-100" data-table="object-list" data-sortable-table data-highlight>
            <thead>               
                    <tr>                       
    <th       
            data-ordering="date"        
        data-searchable
    
    >
        
            Objekt
        
    </th>                      
    <th
        
            data-ordering="date"
        
        data-searchable
        class="sortable"       
    >       
            Name      
    </th>                        
    <th
                    data-ordering="date"
        
        data-searchable
        class="sortable show-for-tablet"
      
    >        
            Adressa
        
    </th>

                        
    <th
        
            data-ordering="date"
        
        data-searchable
        class="sortable show-for-tablet"
        
        
    >
        
            PLZ / Ort
        
    </th>

                        <th></th>
                    </tr>
                
            </thead>
            <tbody>
                       
                                
                                
                      
                    
                        <tr>
                            <td data-ordering-value="mühlebühlstr. 12 wetzikon">
                                
                                    <a href="/de/objects/update/5545">
                                     
                                
                            </td>
                            <td data-ordering-value="neubau mühlbühlstr. 12 zu hd. a3 haustech ag">
                           
                            <td data-ordering-value="mühlebühlstr. 12 " class="show-for-tablet">
                             
                                
                            </td>
                            <td data-ordering-value="8623 wetzikon zh" class="show-for-tablet">
                                
                                    
                            </td>
                            <td class="has-buttons">
                                
                                    
                                    </a>
                                
                                
                                    
                                    <form action="/de/objects/delete/5545" method="post" data-confirm="Wollen Sie das Objekt &quot;Mühlebühlstr. 12&quot; löschen?">
                                        <input type="hidden" name="csrfmiddlewaretoken" value="kZtKwYhpAMJjW05yMffeDiBskTdrHel2UXZXWxAg0WCgIY5GiRKIswGQ2d2bFKq4">
                                        
                                        </button>
                                    </form>
                                
                            </td>
                        </tr>
                    
                
                    
                        <tr>
                            <td data-ordering-value="nb winikon ">
                                
                                    <a href="/de/objects/update/5521">
                                          
                                    </a>
                                
                            </td>
                            <td data-ordering-value="nb winikon z.hd. a3-haustech">
                         
                            </td>
                            <td data-ordering-value="hinterdorfstr. 31 tour ab root" class="show-for-tablet">
                            
                            </td>
                            <td data-ordering-value="6235 winikon" class="show-for-tablet">
                            
                            </td>
                            <td class="has-buttons">
                                
                                    
                                    </a>
                                
                                
                                    
                                    <form action="/de/objects/delete/5521" method="post" data-confirm="Wollen Sie das Objekt &quot;NB Winikon&quot; löschen?">
                                        <input type="hidden" name="csrfmiddlewaretoken" value="kZtKwYhpAMJjW05yMffeDiBskTdrHel2UXZXWxAg0WCgIY5GiRKIswGQ2d2bFKq4">
                                        
                                        </button>
                                    </form>
                                
                            </td>
                        </tr>
                    
                
                    
                        <tr>
                            <td data-ordering-value="neubau oberdorf ">
                                
                                    <a href="/de/objects/update/5516">
                        
                                        
                                    </a>
                                
                            </td>
                            <td data-ordering-value="neubau oberdorf zu hd. a3 haustech ag">
                        
                             
                            <td data-ordering-value="hauptstr. 27 " class="show-for-tablet">
                        
                                
                            </td>
                            <td data-ordering-value="4436 oberdorf bl" class="show-for-tablet">
                                
                            </td>
                            <td class="has-buttons">
                                
                                    
                                    </a>
                                
                                
                                    
                                    <form action="/de/objects/delete/5516" method="post" data-confirm="Wollen Sie das Objekt &quot;Neubau Oberdorf&quot; löschen?">
                                        <input type="hidden" name="csrfmiddlewaretoken" value="kZtKwYhpAMJjW05yMffeDiBskTdrHel2UXZXWxAg0WCgIY5GiRKIswGQ2d2bFKq4">
                                        
                                        </button>
                                    </form>
                                
                            </td>
                        </tr>
                    
                
                    
                        <tr>
                            <td data-ordering-value="nordstrasse ">
                                
                                    <a href="/de/objects/update/4259">
                             
                                        
                                    </a>
                                
                            </td>
                            <td data-ordering-value="nb nordstrasse z.hd. a3-haustech ag">
                          
                            </td>
                            <td data-ordering-value="nordstr. 13 " class="show-for-tablet">
                     
                                
                            </td>
                            <td data-ordering-value="8180 bülach" class="show-for-tablet">
                                
                                
                            </td>
                            <td class="has-buttons">
                                
                               
                                       
                                    </a>
                                
                                
                                    
                                    <form action="/de/objects/delete/4259" method="post" data-confirm="Wollen Sie das Objekt &quot;Nordstrasse&quot; löschen?">
                                        <input type="hidden" name="csrfmiddlewaretoken" value="kZtKwYhpAMJjW05yMffeDiBskTdrHel2UXZXWxAg0WCgIY5GiRKIswGQ2d2bFKq4">
                                        
                                        </button>
                                    </form>
                                
                            </td>
                        </tr>
                    
                
                    
                        <tr>
                            <td data-ordering-value="seerenstr. 12 ">
                                
                                    <a href="/de/objects/update/1350">
                                      
                                        
                                    </a>
                                
                            </td>
                            <td data-ordering-value="neubau seerenstr. 12 zu hd. a3 haustech ag">
                                
                            </td>
                            <td data-ordering-value="seerenstr. 12 " class="show-for-tablet">
                           
                                
                            </td>
                            <td data-ordering-value="8187 weiach" class="show-for-tablet">
                                
                             
                            </td>
                            <td class="has-buttons">
                                
                                   
                                    </a>
                                
                                
                                    
                                    <form action="/de/objects/delete/1350" method="post" data-confirm="Wollen Sie das Objekt &quot;Seerenstr. 12&quot; löschen?">
                                        <input type="hidden" name="csrfmiddlewaretoken" value="kZtKwYhpAMJjW05yMffeDiBskTdrHel2UXZXWxAg0WCgIY5GiRKIswGQ2d2bFKq4">
                                        
                                        </button>
                                    </form>
                                
                            </td>
                        </tr>
                    
                
                    
                        <tr>
                            <td data-ordering-value="siglistorf ">
                                
                                    <a href="/de/objects/update/3960">
                               
                                        
                                    </a>
                                
                            </td>
                            <td data-ordering-value="nb siglistorf z.hd. a3-haustech ag">
                       
                            </td>
                            <td data-ordering-value="kapellenweg 34 " class="show-for-tablet">
                             
                                
                            </td>
                            <td data-ordering-value="5462 siglistorf" class="show-for-tablet">
                                
                            
                            </td>
                            <td class="has-buttons">
                                
                                   
                                    </a>
                                
                                
                                    
                                    <form action="/de/objects/delete/3960" method="post" data-confirm="Wollen Sie das Objekt &quot;Siglistorf&quot; löschen?">
                                        <input type="hidden" name="csrfmiddlewaretoken" value="kZtKwYhpAMJjW05yMffeDiBskTdrHel2UXZXWxAg0WCgIY5GiRKIswGQ2d2bFKq4">
                                       
                                        </button>
                                    </form>
                                
                            </td>
                        </tr>
                    
                
                    
                        <tr>
                            <td data-ordering-value="sulgen moschee ">
                                
                                    <a href="/de/objects/update/4604">
                                       
                                        
                                    </a>
                                
                            </td>
                            <td data-ordering-value="umbau sulgen moschee zu hd. a3 haustech ag">
                                
                            </td>
                            <td data-ordering-value="romanshornstr. 10 " class="show-for-tablet">
                               
                                
                            </td>
                            <td data-ordering-value="8583 sulgen" class="show-for-tablet">
                                
                                    
                            </td>
                            <td class="has-buttons">
                                
                                   
                                    </a>
                                
                                
                                    
                                    <form action="/de/objects/delete/4604" method="post" data-confirm="Wollen Sie das Objekt &quot;Sulgen Moschee&quot; löschen?">
                                        <input type="hidden" name="csrfmiddlewaretoken" value="kZtKwYhpAMJjW05yMffeDiBskTdrHel2UXZXWxAg0WCgIY5GiRKIswGQ2d2bFKq4">
                                       
                                        </button>
                                    </form>
                                
                            </td>
                        </tr>
     
                    <tr class="hide" data-table-noentry>
                        <td colspan="3">
                           
                        </td>
                    </tr>
                
            </tbody>
        </table> --}}

          <table class="rows width-100" data-table="costcenter-list" data-highlight data-sortable-table="kommission_erstellen">
            <thead>
             <tr>                      
                    <th>Objekt</th>
                    <th>Name</th>
                    <th>Adresa</th>
                    <th>PLZ/ORT</th>
                    <th>Veprime</th>
                 </tr>
            </thead>
            <tbody>
    @foreach($objects as $object)
        <tr>
            <td>{{$object->objekt}}</td>
            <td>{{$object->firma}}</td>
            <td>{{$object->adresa}}</td>
            <td>{{$object->ort}}</td>
            <td>
            <a href="{{route('objekt.edit', $object->id)}}"><button><i class="icon icon-edit"></i></button></a>
            <form method="POST" action="{{route('objekt.destroy', $object->id)}}">
                @csrf
                @method('DELETE')
                <button type="submit"><i class="icon icon-delete"></i></button></form>
            </td>
        </tr>
             
        @endforeach
            </tbody>
        </table>
    </section>
        </main>

            
    <footer class="shopfooter">
        
            <section class="container medium padding-top-1 padding-bottom-2">
                
                    <div class="grid-x grid-padding-y">
                        
                            <div class="cell text-center">                             
            </section>
        
    </footer>


    

    

    
    </body>
</html>
