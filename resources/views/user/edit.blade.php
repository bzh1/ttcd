<!DOCTYPE html>

<style>
.headero {
  padding: 20px;
  text-align: center;
  background: grey;
  color: white;
}
.footer {
  padding: 20px;
  text-align: center;
  background: grey;
  }

    </style>
        <html lang="de-CH" class="no-js">
 
    <head>
        <title>KOMMISONI</title>
        <link rel="stylesheet" href="{{asset('css\app.css')}}">
        <link rel="stylesheet" href="{{asset('css\icons.css')}}">
       <meta name="robots" content="all,noodp">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="application-name" content="Elektrogrosshandel">
            <meta name="theme-color" content="#0083C3">
            <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="csrf-token" content="k8E543y1c8pfewXIGW0kjeSwVS8ejVvAU6aiuCRSCiic0uXQcyvO8sXUDcXYhrAC">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/static/images/favicon/apple-touch-icon-57x57.png">
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/images/favicon/apple-touch-icon-114x114.png">
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/images/favicon/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/images/favicon/apple-touch-icon-144x144.png">
            <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/static/images/favicon/apple-touch-icon-60x60.png">
            <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/images/favicon/apple-touch-icon-120x120.png">
            <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/images/favicon/apple-touch-icon-76x76.png">
            <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/images/favicon/apple-touch-icon-152x152.png">
            <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/favicon.ico">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-196x196.png" sizes="196x196">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-96x96.png" sizes="96x96">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-32x32.png" sizes="32x32">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-16x16.png" sizes="16x16">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-128.png" sizes="128x128">   
    <meta name="msapplication-config" content="/static/ieconfig.xml">
    <link rel="manifest" href="/static/manifest.webmanifest"> 
            <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=greek">    
            <link rel="stylesheet" type="text/css" href="/static/css/icons.css?f4a2080209ffae2cdba0e">
            <link rel="stylesheet" type="text/css" href="/static/css/app.css?f4a2080209ffae2cdba0e">      
            <script>              
                var cssLoaded = true;
            </script>
            <script defer src="/static/js/app.js?f4a2080209ffae2cdba0e"></script>
            <link rel="canonical" href="https://www.elektrogrosshandel.ch/de/costcenters/">               
                        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53269130-1"></script>                
                    <script>
                        window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                        gtag('config', 'UA-53269130-1');
                        
                    </script> 
    </head><div class="headero">
  <h1>My Website</h1>
</div>
<header class="shopheader">
<section class="shopheader__middle">             
        </a>
    </div>

                    
                        <div class="shopheader__search">
                                      <form class="input-group inline-button"  style=" float: right;" method="GET" role="search" action="{{route('search.create')}}">
                        @csrf
                        <input class="input-group-field" placeholder="Search" name="search" aria-label="Search" style="margin-top: 5px;">
                        <button class="button clear" type="submit" style="margin-top: 5px;">Search</button>
                      </form> 
                            <div class="shopheader__search-instant-results" data-instant-search-results></div>
                        </div>
                    
                    
                        
                       <section class="shopheader__account">                    
                                    @if(Auth::user()->isadmin)
                                    <a href="{{ url('/admin') }}" data-toggle="account-menu" title="Mein Konto">
                                        <i class="icon icon-user medium"></i>
                                    </a>
                                    @else
                                    <a href="{{ url('/home') }}" data-toggle="account-menu" title="Mein Konto">
                                        <i class="icon icon-user medium"></i>
                                    </a>
                                    @endif
                                    



                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                                            
                                                            <i class="fa fa-sign-out" style="font-size:20px;color:red"></i>
                                                        

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                    </div>




                                      
                                    
                                    <div class="shopheader-menu shopheader-menu--account dropdown-pane widget"
                                         id="account-menu"
                                         data-dropdown
                                         data-hover="true"
                                         data-hover-pane="true"
                                         data-position="bottom"
                                         data-alignment="right">

                                        <div class="widget__heading">
                     <form method="POST" class="width-100" action="/de/account/logout/">
                                                    <input type="hidden" name="csrfmiddlewaretoken" value="QAtYp5OejKarMVrbXLYOQpQwNCxTCyugqyZbPE75JU3oyTrjtntiFDVUvWmDA4zi">
                                                    <button type="submit" class="button hollow margin-top-1 width-100">
                                                        <i class="icon icon-logout medium"></i>
                                                        <span>Abmelden</span>
                                                    </button>
                                                </form>
                                                
                                            
                                        </div>
                                    </div>
                                
                            
                            
                              
                                   <a
                                    href="{{ route('cart.index') }}"
                                    title=" "
                                    data-toggle="cart-menu"
                                    data-bind-prop-set="cart:activeCartLabel,title=Warenkorb {}">
                                
                                    <i class="icon icon-cart medium"></i>
                                    <a href="html.html">
                                    <span class="badge hide" data-bind-text="cart:activePositionCount" data-bind-class="cart:isActiveEmpty,hide">
                                      
                                    </span>
                                </a>
                                <div
                                    class="shopheader-menu shopheader-menu--cart dropdown-pane widget "
                                    
                                    data-bind-prop-set="cart:hasActivePositions,data-cart-preview=/de/cart/preview"
                                    data-cart-preview=/de/cart/preview
                                    id="cart-menu"
                                    data-dropdown
                                    data-hover="true"
                                    data-hover-pane="true"
                                    data-position="bottom"
                                    data-alignment="right"
                                >
                                    <div class="cart-preview" data-cart-preview-widget>
                                        <div class="cart-preview__loader"><i class="icon loader"></i></div>
                                        <div
                                            class="cart-preview__content"
                                            data-bind-innerhtml="cart:preview"
                                        ></div>
                                    </div>
                                </div>
                            
                        </section>
                    
                </div>
            </section>
                                            </div>
                                                
                                            </div>
                                        </div>
            
                </div>
            </nav>
        
    </header> 

        <header class="print-header grid-x">
    
        <div class="cell small-5">
            <img src="/static/images/logo.svg" class="print-header__logo">
        </div>
    
    
        <div class="cell small-7">
            <div class="grid-x grid-margin-x">
                <div class="cell auto">
                  
                </div>
                   </div>
            </div>
        </div>
    
</header>


        <header class="print-header grid-x">
    
        <div class="cell small-5">
            <img src="/static/images/logo.svg" class="print-header__logo">
        </div>
    
    
        <div class="cell small-7">
            <div class="grid-x grid-margin-x">
                <div class="cell auto">
                    
                        
                            &lt;div class=&quot;grid-x grid-margin-x grid-margin-y&quot;&gt;<br>&lt;div class=&quot;cell small-12 tablet-4&quot;&gt;<br>&lt;h4&gt;A. Saesseli &amp; Co. AG&lt;/h4&gt;<br>Pflanzschulstrasse 17&lt;br&gt;<br>8400 Winterthur&lt;br&gt;<br>&lt;br&gt;<br>Tel. &lt;a href=&quot;tel:+41522352626&quot;&gt;+41 52 235 26 26&lt;/a&gt;&lt;br&gt;<br>&lt;a href=&quot;mailto:verkauf@saesseli.ch&quot;&gt;verkauf@saesseli.ch&lt;/a&gt;<br>&lt;/div&gt;<br>&lt;div class=&quot;cell small-12 tablet-4&quot;&gt;<br>&lt;h4&gt;EL Kabel AG&lt;/h4&gt;<br>Leisibachstrasse 9&lt;br&gt;<br>6307 Root&lt;br&gt;<br>&lt;br&gt;<br>Tel. &lt;a href=&quot;tel:+41414555070&quot;&gt;+41 41 455 50 70&lt;/a&gt;&lt;br&gt;<br>&lt;a href=&quot;mailto:verkauf@elkabel.ch&quot;&gt;verkauf@elkabel.ch&lt;/a&gt;<br>&lt;/div&gt;<br>&lt;div class=&quot;cell small-12 tablet-4&quot;&gt;<br>&lt;h4&gt;Standard AG&lt;/h4&gt;<br>Freulerstrasse 6&lt;br&gt;<br>4127 Birsfelden&lt;br&gt;<br>&lt;br&gt;<br>Tel. &lt;a href=&quot;tel:+41613788200&quot;&gt;+41 61 378 82 00&lt;/a&gt;&lt;br&gt;<br>&lt;a href=&quot;mailto:verkauf@standard.ch&quot;&gt;verkauf@standard.ch&lt;/a&gt;<br>&lt;/div&gt;<br>&lt;/div&gt;
                        
                    
                </div>
                <div class="cell auto">
                    
                        
                            info@elektrogrosshandel.ch<br>
                        
                        
                    
                </div>
            </div>
        </div>
    
</header>

    

    
        <main id="app" class="">          
                <h1 style ="text-align:center;">
    


</h1>
            
            
        </div>
<div class="container medium padding-vertical-3">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div>
                

                <div class="container medium padding-vertical-3">
                    <form method="POST" action="{{ route('user.update') }}">
                        @csrf
                        @if(session('success'))
                            <div class="alert alert-success" role="success"></div>
                            {{session('success')}}
                        @endif
                            <h1> Update Profile </h1>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Your name') }}</label>

                            <div class="col-md-6">
                                <input class="uname" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user['name'] }}" required autocomplete="name" placeholder="Valon" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Your email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="valon123@gmail.com" value="{{ $user['email'] }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                       




                        <div class="form-group row">
                            <label for="firma" class="col-md-4 col-form-label text-md-right">{{ __('Firma') }}</label>

                            <div class="col-md-6">
                                <input @error('firma') is-invalid @enderror id="firma" type="text" name="firma"  required autocomplete="firma" value="{{ $user['firma'] }}" autofocus>

                                @error('firma')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>





                         <div class="form-group row">
                            <label for="rruga" class="col-md-4 col-form-label text-md-right">{{ __('Rruga') }}</label>

                            <div class="col-md-6">
                                <input @error('rruga') is-invalid @enderror id="rruga" type="text" name="rruga"  required autocomplete="rruga" value="{{ $user['rruga'] }}" autofocus>
                                @error('rruga')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="nr" class="col-md-4 col-form-label text-md-right">{{ __('Nr i identifikimit') }}</label>

                            <div class="col-md-6">
                                <input @error('nr') is-invalid @enderror id="nr" type="text" name="nr"  required autocomplete="nr" value="{{ $user['nr'] }}" autofocus>
                                @error('nr')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 



                         <div class="form-group row">
                            <label for="telefon" class="col-md-4 col-form-label text-md-right">{{ __('Telefon') }}</label>

                            <div class="col-md-6">
                                <input @error('telefon') is-invalid @enderror id="telefon" type="text" name="telefon"  required autocomplete="telefon" value="{{ $user['telefon'] }}" autofocus>
                                @error('telefon')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="biznesi" class="col-md-4 col-form-label text-md-right">{{ __('Biznesi') }}</label>

                            <div class="col-md-6">
                                <input @error('biznesi') is-invalid @enderror id="biznesi" type="text" name="biznesi"  required autocomplete="biznesi" value="{{ $user['biznesi'] }}" autofocus>
                                @error('biznesi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="emri" class="col-md-4 col-form-label text-md-right">{{ __('Emri') }}</label>

                            <div class="col-md-6">
                                <input @error('emri') is-invalid @enderror id="emri" type="text" name="emri"  required autocomplete="emri" value="{{ $user['emri'] }}" autofocus>
                                @error('emri')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mbiemri" class="col-md-4 col-form-label text-md-right">{{ __('mbiemër') }}</label>

                            <div class="col-md-6">
                                <input @error('mbiemri') is-invalid @enderror id="mbiemri" type="text" name="mbiemri"  required autocomplete="mbiemri" value="{{ $user['mbiemri'] }}" autofocus>
                                @error('mbiemri')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="emailp" class="col-md-4 col-form-label text-md-right">{{ __('E-mail personal') }}</label>

                            <div class="col-md-6">
                                <input @error('emailp') is-invalid @enderror id="emailp" type="text" name="emailp"  required autocomplete="emailp" value="{{ $user['emailp'] }}" autofocus>
                                @error('emailp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>







                        <div class="form__footer form__footer--borderless">
                            <div class="form__footer__col">
                                <button type="submit" class="button expanded ">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


        </main>                                <div class="footer">
  <strong>Footer</strong>
</div>
 
    </body>
</html>