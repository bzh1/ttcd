<!DOCTYPE html>
<html>
<head>
	<title>Checkout</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" /> 
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />

    <style type="text/css">
        #invoice{
    padding: 30px;
}

.invoice {
    position: relative;
    background-color: #FFF;
    min-height: 680px;
    padding: 15px
}

.invoice header {
    padding: 10px 0;
    margin-bottom: 20px;
    border-bottom: 1px solid #3989c6
}

.invoice .company-details {
    text-align: right
}

.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    margin-bottom: 20px
}

.invoice .invoice-to {
    text-align: left
}

.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #3989c6
}

.invoice main {
    padding-bottom: 50px
}

.invoice main .thanks {
    margin-top: -100px;
    font-size: 2em;
    margin-bottom: 50px
}

.invoice main .notices {
    padding-left: 6px;
    border-left: 6px solid #3989c6
}

.invoice main .notices .notice {
    font-size: 1.2em
}

.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

.invoice table td,.invoice table th {
    padding: 15px;
    background: #eee;
    border-bottom: 1px solid #fff
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #3989c6;
    font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
    text-align: right;
    font-size: 1.2em
}

.invoice table .no {
    color: #fff;
    font-size: 1.6em;
    background: #3989c6
}

.invoice table .unit {
    background: #ddd
}

.invoice table .total {
    background: #3989c6;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}

.invoice table tfoot td {
    background: 0 0;
    border-bottom: none;
    white-space: nowrap;
    text-align: right;
    padding: 10px 20px;
    font-size: 1.2em;
    border-top: 1px solid #aaa
}

.invoice table tfoot tr:first-child td {
    border-top: none
}

.invoice table tfoot tr:last-child td {
    color: #3989c6;
    font-size: 1.4em;
    border-top: 1px solid #3989c6
}

.invoice table tfoot tr td:first-child {
    border: none
}

.invoice footer {
    width: 100%;
    text-align: center;
    color: #777;
    border-top: 1px solid #aaa;
    padding: 8px 0
}

@media print {
    .invoice {
        font-size: 11px!important;
        overflow: hidden!important
    }
    .no-print{
        display: none;
    }

    .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    }

    .invoice>div:last-child {
        page-break-before: always
    }
}
    </style>
</head>
<body>
	<div class="toplink-contaner no-print">
                    <div class="container">
                        <div class="top-link-inner">
                            <div class="box-left">
                                <p class="welcome-msg"> </p>
                                <div class="block-header">
                                    <div class="phone"><label> </label>
                                        <p>2233665588</p>
                                    </div>
                                    <div class="email"><label></label>
                                        <p>emailiadminit@gmail.com</p>
                                    </div>
                                </div>
                            </div>
                               <div class="box-right">
                                <div class="language-currency">
                                </div>
                                
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/admin')}}" title="Home">Home</a></div>
                                </div>
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('adminarticles.index')}}" title="Home">Articles</a></div>
                                </div> 
                                @else
                                @endif

                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                      <div class="top-link" id="top-link">
                                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                           <i class="select-lang"> Log out</i>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form></a>
                                        </div>
                                </div>
                                @else 
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('login' )}}" title="Home">Log in</a></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--Author      : @arboshiki-->
<div id="invoice">

    
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                        
                            <img src="{{asset('img/logo.jpg')}}" data-holder-rendered="true" />
                            
                    </div>
                    <div class="col company-details">
                        <h2 class="name">
                            
                            ImpressLight
                            
                        </h2>
                        <div>Adresa E Kompanise</div>
                        <div>(123) 456-789</div>
                        <div>company@example.com</div>
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">Fatura Per:</div>
                        <h2 class="to">Emri: {{Auth::user()->emri}}</h2>
                        <div class="address">Adresa:  {{ Auth::user()->rruga}}</div>
                        <div class="email">Emaili {{Auth::user()->email,}}</div>
                        <div class="number">Phone Number {{Auth::user()->telefon,}}</div>
                    </div>
                    
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">PRODUKTI</th>
                            
                            <th class="text-right">SASIA</th>
                            <th class="text-right">Qmimi</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @if(Cart::count() > 0)
                    @foreach($carts as $cart)
                        <tr>
                            <td class="no"><img style="width: 150px; height: 150px;" src="{{asset('storage/' . $cart->options->image)}}"></td>
                            <td style="max-width: 45px;" class="text-left"><h3>{{ $cart->name }}</h3>{{ $cart->options->desc }}</td>
                            
                            <td class="qty">{{$cart->qty}}</td>
                            <td class="total">{{ $cart->price }}$</td>
                        </tr>
                        @endforeach
                     @else
            <div class="row">
                <div class="col-sm-6 col-md-offset-3 col-sm-offset-3">
                    <h2>No Articles in the cart</h2>
                </div>
            </div>
        @endif
                        
                        
                    </tbody>
                    <tfoot>
                        <tr></tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">Qmimi Total</td>
                            <td>${{Cart::subtotal()}}</td>
                        </tr>
                    </tfoot>
                </table>
                
                <div class="notices no-print">
                    <div>Njoftim:</div>
                    <div class="notice">Ju lutemi shikoni a e keni adresen dhe numrin e telefonit mire dhe shtypeni checkout per te perfunduar.</div>
                    <button style="float: right; font-size: 16px; margin: 20px;" id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Ruaj Si PDF</button>
                    <form action="{{route('orders.store')}}" method="POST" enctype="multipart/form-data">
                        <button class="btn btn-danger"  type="submit" style="font-size: 16px; margin: 20px;  margin-left: 30px;" >Checkout</button>
                </div>
            </main>
            
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>
<script type="text/javascript">
     $('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });
</script>


                
<div>
	
	
        
		@csrf
        <div style="display: none;">
	<div class="form-group">
    	<label for="name">Article Name</label>
    	<input type="text" class="form-control" name="article_name" placeholder="Name" value="@foreach($carts as $cart) {{ $cart->name}} ,  @endforeach" readonly>
        @error('article_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
  	</div>
  	<div class="form-group">
    	<label for="company">User Name</label>
    	<input type="text" class="form-control" name="user_name" placeholder="user_name" value="{{ Auth::user()->name}}" readonly>
          @error('user_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
  	</div>
  	<div class="form-group">
    	<label for="company">Address</label>
    	<input type="text" class="form-control" name="address" placeholder="Address" value="{{ Auth::user()->rruga}}" readonly>
          @error('address')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
  	</div>
     <div class="form-group">
        <label for="company">Quantity</label>
        <input type="text" class="form-control" name="qty" placeholder="Address" value="@foreach($carts as $cart) {{ $cart->qty}} ,  @endforeach" readonly>
          @error('qty')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
    </div>   
    <div class="form-group">
        <label for="company">Telefon</label>
        <input type="text" class="form-control" name="telefon" placeholder="Address" value="{{ Auth::user()->telefon}}" readonly>
          @error('telefon')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="company">Total Price</label>
        <input type="integer" class="form-control" name="price" placeholder="Address" value="{{Cart::subtotal()}}" readonly>
        </div>
          @error('price')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
    </div>
		
	</form>
</div>	 









</body>
</html>


