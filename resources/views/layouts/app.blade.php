<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <style>
.header {
  padding: 0.5px;
  text-align: center;
  background: grey;
  color: white;
}
.footer {
  padding: 30px;
  text-align: center;
  background: grey;
  }
  </style>


    
        <html lang="de-CH" class="no-js">
    <div class="header">
  <h1>My Website</h1>

</div>


   

        <link rel="stylesheet" href="{{asset('css\app.css')}}">
        <link rel="stylesheet" href="{{asset('css\icons.css')}}">
       <link rel="stylesheet" href="{{asset('css\_settings.css')}}">
        
            <meta name="robots" content="all,noodp">
        

        
        
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="application-name" content="Elektrogrosshandel">
            <meta name="theme-color" content="#0083C3">
            <meta name="apple-mobile-web-app-capable" content="yes">
        

        

        
        <meta name="csrf-token" content="Zth2Mn80Xjct2rXSdxB75J1Fu7JCPD9JBJ6QO2XdEu5GFXreZ2ufuTHwOZBRr77H">

        
        
    
            <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/static/images/favicon/apple-touch-icon-57x57.png">
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/images/favicon/apple-touch-icon-114x114.png">
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/images/favicon/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/images/favicon/apple-touch-icon-144x144.png">
            <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/static/images/favicon/apple-touch-icon-60x60.png">
            <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/images/favicon/apple-touch-icon-120x120.png">
            <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/images/favicon/apple-touch-icon-76x76.png">
            <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/images/favicon/apple-touch-icon-152x152.png">

            <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/favicon.ico">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-196x196.png" sizes="196x196">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-96x96.png" sizes="96x96">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-32x32.png" sizes="32x32">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-16x16.png" sizes="16x16">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-128.png" sizes="128x128">
        

    <meta name="msapplication-config" content="/static/ieconfig.xml">
    <link rel="manifest" href="/static/manifest.webmanifest">


        
        
            <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=greek">
        

        
            <link rel="stylesheet" type="text/css" href="/static/css/icons.css?f4a2080209ffae2cdba0e">
            <link rel="stylesheet" type="text/css" href="/static/css/app.css?f4a2080209ffae2cdba0e">
        

        
        
            <script>
                
                
                
                var cssLoaded = true;
            </script>
            <script defer src="/static/js/app.js?f4a2080209ffae2cdba0e"></script>
        

        
            
            
                <link rel="canonical" href="https://www.elektrogrosshandel.ch/de/account/login/?next=/de/">
            
        

        
        
            
                
                    
                        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53269130-1"></script>
                    
                    <script>
                        window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                        gtag('config', 'UA-53269130-1');
                        
                    </script>
                
            
</head>
<body>
<!-- 
     <div data-flashmessages class="flashmessages">             
                <form method="get" class="shopheader-mobile__search__form" action="/de/search/" data-mobile-search>
                    <input
                        autocomplete="off"
                        class="shopheader-mobile__search__field input-group-field"
                        name="q"
                        placeholder="Produkt Suche / Artikel-Nr. / Hersteller"
                        required
                        type="search"
                        value=""
                        data-mobile-search-input
                    >
                   

                    
                                
                            
                    
                                
                                    
                                        
<li class="hamburger-item">
    <div class="hamburger-item__link">
        
            <span></span>
        
    </div>
    
        <a tabindex="0"></a>
    
    
        <ul class="vertical menu">
            <li class="hamburger-item is-header js-drilldown-back">
                <div class="hamburger-item__link">
                    <a tabindex="0">
                       
                    </a>
                </div>
            </li>                                        
                                            <span class="shopheader__link shopheader__link--nested"
                                                  data-toggle="subnav-5c77fb44d76f74001fa323d5"
                                            >
                                               
                                            </span>
                                        
                                    
                                    
                                        <div
                                            id="subnav-5c77fb44d76f74001fa323d5"
                                            class="shopheader__subnav dropdown-pane shopheader__subnav--nested"
                                            data-dropdown
                                            data-hover="true"
                                            data-hover-pane="true"
                                            data-close-on-click="true"
                                        >
                                            <div class="container grid-container">
                                                
                                                    <div class="grid-x grid-margin-x small-up-2 phablet-up-3 tablet-up-4 laptop-up-5">
                                                        
                                                            
                                                                <div class="cell">
                                                                    
                                                                        <span class="shopheader__subnav__link h3 separator-left">Elektrogrosshandel</span>
                                                                    
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/firma/wer-sind-wir/" target="_self">wer sind wir ?</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/firma/history/" target="_self">Geschichte</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/firma/team/" target="_self">Mitarbeiter</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/firma/wirsuchen/" target="_self">Jobs</a>
                                                                        
                                                                    
                                                                </div>
                                                            
                                                        
                                                            
                                                                <div class="cell">
                                                                    
                                                                        <a class="shopheader__subnav__link h3 separator-left" href="/de/unternehmen/asco/" target="_self">A. Saesseli &amp; Co. AG</a>
                                                                    
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/asco/#kontakt" target="_self">Kontakt</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/asco/#offnungszeiten" target="_self">Öffnungszeiten</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/asco/#standort" target="_self">Standort</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/asco/mitarbeiter/" target="_self">Mitarbeiter</a>
                                                                        
                                                                    
                                                                </div>
                                                            
                                                        
                                                            
                                                                <div class="cell">
                                                                    
                                                                        <a class="shopheader__subnav__link h3 separator-left" href="/de/unternehmen/elka/" target="_self">EL Kabel AG</a>
                                                                    
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/elka/#kontakt" target="_self">Kontakt</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/elka/#offnungszeiten" target="_self">Öffnungszeiten</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/elka/#standort" target="_self">Standort</a>
                                                                        
                                                                    
                                                                        
                                                                            <a class="shopheader__subnav__link" href="/de/unternehmen/elka/mitarbeiter/" target="_self">Mitarbeiter</a>
                                                                        
                                                                    
                                                                </div>
                                                            
                                                        
                                                            
                                                              
                                                        
                                                    </div>
                                                
                                            </div>
                                        </div>
                                    
                                
                            
                        
                    
                </div>
            </nav>
         -->
    <div id="app">
        

        <main class="py-4">
            @yield('content')
        </main>
    </div>
         <div class="footer">
  <strong>  &copy  THE THECH TERMS COMPUTER DIRECTORY</strong>
        </div>
</body>
</html>
