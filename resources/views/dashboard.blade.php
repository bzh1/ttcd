<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Impresslight</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />
    <script type="text/javascript" src="../widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js"></script>
    <style type="text/css">
    .toplink-contaner {
        opacity: 100%;
        
    }

    #log-in {

        background-color: white;
        padding: 8px;
        color: black;
        text-decoration: none;
        margin: 10px;
        border-radius: 8px;
    }

    #mobile {
        display: none;
    }

    @media (max-width: 640px) {
        #mobile {
            display: block;
            background-color: black;
            opacity: 50%;
            padding: 10px;

        }
        #mobilee {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
            background-color: black;

        }

        #desktop {
            display: none;
        }
    }

    .header {
        margin-left: 10px;
    }

    </style>
</head>

<body class=" cms-index-index cms-assyrian-home cms-assyrian-home">
    <div class="wrapper">
        <div class="page">
            <div id="wrapper">
                <div id="loading" style="position: fixed;top: 50%;left: 50%;margin-top: -50px;"></div>
            </div>
            <div class="header-container">
                <div class="toplink-contaner" id="desktop">
                    <div class="container">
                        <div class="top-link-inner">
                            <div class="box-left">
                                <p class="welcome-msg"> </p>
                                <div class="block-header">
                                    <div class="phone">
                                        <p style=" font-size: 12px; vertical-align: middle;"> +49-123-456</p>
                                    </div>
                                    <div class="email">
                                        <p style="font-size: 12px;"> EMAILIADMINIT@HOTMAIL.COM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-right">
                                <div class="language-currency">
                                </div>
                                @if (Auth::check())
                                @if(Auth::user()->isadmin)
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/admin')}}" title="Home">@lang('messages.home')</a></div>
                                </div>
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('adminarticles.create')}}" title="Home">@lang('messages.createArticle')</a></div>
                                </div>
                                @else
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/home')}}" title="Home">@lang('messages.home')</a></div>
                                </div>
                                @endif
                                @else
                                @endif
                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                    <div class="top-link" id="top-link">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="select-lang"> @lang('messages.logout')</i>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                    </div>
                                </div>
                                @else
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a id="log-in" href="{{ route('login' )}}" title="Home">@lang('messages.login')</a></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="mobile">
                    <div class="col">

                        @if (Auth::check())
                        <div class="top-link" id="top-link">
                            <div class="top-link" id="top-link">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="select-lang"> Log out</i>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </div>
                        </div>
                        @else
                        <div class="top-link" id="top-link">
                            <div class="select-lang"><a id="log-in" href="{{ route('login' )}}" title="Home">@lang('messages.login')</a></div>
                        </div>
                        @endif
                    </div>
                    <div class="col">
                        <h2 style="color: white; text-align: center;">ImpressLight</h2>

                    </div>
                </div>
            </div>
            <div class="header" id="mobilee desktop">
                <div class="container">
                    <div class="header-content">
                        <div class="logo-container">
                            <h1 class="logo"><a href="#" title="online-lighting-shop.com" class="logo"><img src="{{asset('img/logo.jpg')}}" style="width:200px;" /></a></h1>
                        </div>
                    <div style="float: right; ">
                               <?php $lang =app()->getLocale(); ?>
                <select onchange="location = this.value;" class="text-uppercase" id="languageCurrent">
                    <option @if($lang == 'de') selected @endif value="/locale/de">DE</option>      
                    <option @if($lang == 'en') selected @endif value="/locale/en">EN</option>      
                    <option @if($lang == 'fr') selected @endif value="/locale/fr">FR</option>
                 </select>
                 </div>
                        </div>
            </div>
        </div>
                    </form>
                    <div class="top-cart-wrapper">
                        <div class="top-cart-contain">
                            <script type="text/javascript">
                            $jq(document).ready(function() {
                                var enable_module = $jq('#enable_module').val();
                                if (enable_module == 0) return false;
                            })

                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
            $jq(window).scroll(function() {
                if ($jq(this).scrollTop() > 40) {
                    $jq('.header').addClass("fix-header");
                } else {
                    $jq('.header').removeClass("fix-header");
                }
            });

            </script>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <h1 style="text-align:center;"> @lang('messages.hause')</h1>
                            <a href="{{ route('hause.index') }}">
                                <img src="{{asset('img/hause.jpg')}}" style="width:100%">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <h1 style="text-align:center;"> @lang('messages.buro')</h1>
                            <a href="{{ route('buros.index') }}">
                                <img src="{{asset('img/buro.jpg')}}" alt="Nature" style="width:100%;">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <h1 style="text-align:center;"> @lang('messages.baus')</h1>
                            <a href="{{ route('baus.index') }}">
                                <img src="{{asset('img/baustelle.jpg')}}" alt="Fjords" style="width:47%;">
                                <div class="caption">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   -->
    </div>

</html>
</body>
