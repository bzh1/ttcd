<!DOCTYPE html>
<html>

<head>
    <title>Order By Plan</title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
    .button {
        background-color: #4CAF50;
        /* Green */
        border: none;
        color: white;
        padding: 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        margin: 10px 2px;
        cursor: pointer;
        border: none;
        color: white;
        padding: 12px 16px;
        font-size: 16px;
        cursor: pointer;
        text-decoration: none;
  cursor: pointer;
  transition: all 0.3s ease-out;
    }

    .button:hover {
        background-color: darkGreen;
    }

    .btn {
        background-color: DodgerBlue;
        border: none;
        color: white;
        padding: 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        margin: 10px 2px;
        cursor: pointer;
        border: none;
        color: white;
        padding: 12px 16px;
        font-size: 16px;
        cursor: pointer;
    }

    /* Darker background on mouse-over */
    .btn:hover {
        background-color: RoyalBlue;
    }



    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
    }

    .overlay:target {
        visibility: visible;
        opacity: 1;
    }

    .popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 30%;
        position: relative;
        transition: all 5s ease-in-out;
    }

    .popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
    }

    .popup .close {
        position: absolute;
        top: 20px;
        right: 30px;
        transition: all 200ms;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #333;
    }

    .popup .close:hover {
        color: #06D85F;
    }

    .popup .content {
        max-height: 30%;
        overflow: auto;
    }

    @media screen and (max-width: 700px) {
        .popup {
            width: 70%;
        }
    }


    .overlayy {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlayy:target {
  visibility: visible;
  opacity: 1;
}

.popupp {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 30%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popupp h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popupp .closee {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popupp .closee:hover {
  color: #06D85F;
}
.popupp .contentt {
  max-height: 30%;
  overflow: auto;
}

@media screen and (max-width: 700px){
  
  .popupp{
    width: 70%;
  }
}
    

    </style>
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="{{asset('img/logo.jpg')}}" alt="impres-light" height="50px" width="200px">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Order By Plan
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/cart">Shporta</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container">
        <h1 class="mt-4">Shto Projekt te ri ose vazhdo me projektin e peparshem</h1>

        <a class="button" href="#popup2"><i class="fa fa-home"> Projektet</i></a><br>
        <a class="btn" href="#popup1"><i class="fa fa-plus"></i> Shto Projekt te Ri</a>
        <p>Ne kete faqe ju mund te beni porosi duke vendosur drita ne planin e shtepis tuaj dhe gjdo drit qe ju e vendosni do te futet ne shporten tuaj dhe ne fund do beni pagesen</p>
    </div>
    <!-- /.container -->
    <div id="popup1" class="overlay">
        <div class="popup">
            <h3>Ploteso Te Dhenat Me Poshte</h3>
            <a class="close" href="#">&times;</a>
            <div class="content">
                <form action="/editori">
                    <label for="fname">Emri Projektit:</label><br>
                    <input type="text" id="fname" name="fname"><br>
                    <label for="Adressa">Adressa:</label><br>
                    <input type="text" id="Adressa" name="Adressa"><br>
                    <label for="Plz">Plz:</label><br>
                    <input type="text" id="Plz" name="Plz"><br>
                    <label for="Ort">Ort:</label><br>
                    <input type="text" id="Ort" name="Ort"><br>
                    <label for="Kommission">Kommission:</label><br>
                    <input type="text" id="Kommission" name="Kommission"><br>
                    <label for="Telefon">Telefon:</label><br>
                    <input type="text" id="Telefon" name="Telefon"><br>
                    <label for="Project-Menager">Project Menager:</label><br>
                    <input type="text" id="Project-Menager" name="Project-Menager"><br><br>
                    <input type="submit" value="Submit" class="button">

                </form>
                <br>
                <p>Tani Do Ju Drejtojme Ne Faqen E Planit</p>
            </div>
        </div>
    </div>

            <div id="popup2" class="overlayy">
	<div class="popupp">
		<h2>Ju keni 0 Projekte</h2>
		<a class="closee" href="#">&times;</a>
		<div class="contentt">
			Per momentin nuk e keni asnje projekt shkoni te faqja tjeter per te filluar nje projekt.
		</div>
	</div>
</div>






</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>

</html>