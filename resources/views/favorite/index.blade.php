<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FUR ZUHAUSE</title>
    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/shop-homepage.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <style type="text/css">
    #desktop {
        display: block;
    }

    #mobile {
        display: none;
    }

    @media screen and (max-width: 768px) {

        #desktop {
            display: none;
        }

        #mobile {
            display: block;
        }

        .top-cart-title {
            margin-right: 300px;
        }

        .form-inline {
            margin-right: 200px
        }

    }

    </style>
</head>

<body>
    <div class="toplink-contaner">
        <div class="container">
            <div class="top-link-inner">
                <div class="box-left">
                    <p class="welcome-msg"> </p>
                    <div class="block-header" id="desktop">
                        <div class="phone"><label> </label>
                            <p>+45 252552525</p>
                        </div>
                        <div class="email"><label></label>
                            <p>emailiadminit@hotmail.com</p>
                        </div>
                    </div>
                </div>
                <div class="box-right">
                    <div class="language-currency">
                    </div>
                    <div class="top-link" id="top-link">
                        <div class="select-lang"><a href="{{ url('/')}}" title="Home">Shopi</a></div>
                    </div>
                    @if (Auth::check())
                    @if(Auth::user()->isadmin)
                    <div class="top-link" id="top-link">
                        <div class="select-lang"><a href="{{ url('/admin')}}" title="Home">Home</a></div>
                    </div>
                    <div class="top-link" id="top-link">
                        <div class="select-lang"><a href="{{ route('adminarticles.create')}}" title="Home">Create Article</a></div>
                    </div>
                    @else
                    <div class="top-link" id="top-link">
                        <div class="select-lang"><a href="{{ url('/home')}}" title="Home">Home</a></div>
                    </div>
                    @endif
                    @else
                    @endif
                    @if (Auth::check())
                    <div class="top-link" id="top-link">
                        <div class="top-link" id="top-link">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="select-lang"> Log out</i>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </div>
                    </div>
                    @else
                    <div class="top-link" id="top-link">
                        <div class="select-lang"><a href="{{ route('login' )}}" title="Home">Log in</a></div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">ImpressLight</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ Request::is('hause*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('hause.index') }}">FUR ZUHAUSE <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item {{ Request::is('buros*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('buros.index') }}">FUR DAS BURO</a>
                </li>
                <li class="nav-item {{ Request::is('baus*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('baus.index') }}">FUR DIE BAUSTELLE</a>
                </li>
            </ul>
            <li>
                <form class="form-inline my-2 my-lg-0" style=" float: right;" method="GET" role="search" action="{{route('search.create')}}">
                    @csrf
                    <input class="form-control mr-sm-2" placeholder="Search" name="search" aria-label="Search" style="margin-top: 5px;">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" style="margin-top: 5px;">Search</button>
                </form>
            </li>
            <li>
                <div id="mini_cart_block" style="color: white; float: right; margin-right: 10px" class="items">
                    <div class="block-cart mini_cart_ajax">
                        <div class="block-cart">
                            <div class="top-cart-title">
                                <a href="{{ route('cart.index') }}">
                                    <span class="badge">{{ Cart::count()}}</span>
                                </a>
                            </div>
                            <div class="top-cart-content">
                                <p class="empty">You have no items in your shopping cart.</p>
                                <div class="top-subtotal">Subtotal: <span class="price">€0.00</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <!-- /.col-lg-3 -->
            <div class="col-lg-9">
                <div>
                    <h1>Wishlist:</h1>
                </div>
                <hr>
                <div class=" table-content table-responsive">
                    @if($articles->count() > 0)
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="plantmore-product-remove">Remove</th>
                                <th class="plantmore-product-thumbnail">Images</th>
                                <th class="cart-product-name">Product</th>
                                <th class="plantmore-product-price">Article Price</th>
                                <th class="plantmore-product-add-cart">Add to cart</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($articles as $article)
                            <tr>
                                <td class="plantmore-product-remove btn btn-danger"><a href="{{route('favorite.show' ,$article->id)}}">Remove</a></td>
                                <td class="plantmore-product-thumbnail"><a href="{{asset('storage/' . $article->image)}}"><img alt="" src="{{asset('storage/' . $article->image)}}" width="90px" ></a></td>
                                <td class="plantmore-product-name">{{$article->name}}</td>
                                <td class="plantmore-product-price"><span class="amount">{{$article->price}}$</span></td>
                                <td class="plantmore-product-add-cart btn btn-success"><a href="{{route('cart.edit' ,$article->id)}}">add to cart</a></td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else 
                        <h2>No articles in your wishlist.</h2>
                        <br>
                    @endif
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-lg-9 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; ImpressLight 2020</p>
        </div>
        <!-- /.container -->
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- deri qetu -->

    </html>
