

</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<style>
    .warning {background-color: #ff9800;}
.warning:hover {background: #e68a00;}
div.gallery {
  margin: 5px;
  border: 1px solid #ccc;
  float: left;
  width: 180px;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
}
#my_centered_buttons { display: flex; justify-content: center; }
</style>
<!-- Mirrored from www.online-lighting-shop.com/wall-lamps by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 29 Mar 2020 22:12:36 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript">
    

    </script>
    <title>Shopping Cart</title>
    <meta name="description" content="Buy your Wall lamps Online! Modern wall lamps, Classic wall lamps, LED wall lamps, Halogen wall lamps etc." />
    <meta name="keywords" content="lamps, lights, lamps online, lights online" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <link rel="icon" href="skin/frontend/assyrian/uk/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="skin/frontend/assyrian/uk/favicon.ico" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap.css')}}" media="all" /> 
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\bootstrap-theme.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\styles.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\owl.carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\base\default\css\widgets.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\animate.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\cs\category.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\aw_autorelated\css\product.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\amshopby.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\magentothem\css\ma_quickview.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\skin.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('css\skin\frontend\assyrian\uk\css\print.css')}}" media="print" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

   
    <script type="text/javascript" src="../widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js"></script>
</head>

<body class=" catalog-category-view categorypath-wall-lamps category-wall-lamps">  
    <div class="wrapper">
        
        <div class="page">
            <div id="wrapper">
                <div id="loading" style="position: fixed;top: 50%;left: 50%;margin-top: -50px;"></div>
            </div>
            <div class="header-container">
                <div class="toplink-contaner">
                    <div class="container">
                        <div class="top-link-inner">
                            <div class="box-left">
                                <p class="welcome-msg"> </p>
                                <div class="block-header">
                                    <div class="phone"><label> </label>
                                        <p style="font-size: 14px;">+45 23232323223</p>
                                    </div>
                                    <div class="email"><label></label>
                                        <p>emailiadminit@gmail.com</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-right">
                                <div class="language-currency">
                                </div>
                              @if (Auth::check())
                                @if(Auth::user()->isadmin)
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/admin')}}" title="Home">Home</a></div>
                                </div>
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('adminarticles.create')}}" title="Home">Create Article</a></div>
                                </div>
                                @else
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ url('/home')}}" title="Home">Home</a></div>
                                </div> 
                                @endif
                                @else
                                @endif

                                @if (Auth::check())
                                <div class="top-link" id="top-link">
                                      <div class="top-link" id="top-link">
                                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                           <i class="select-lang"> Log out</i>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                        </div>
                                </div>
                                @else 
                                <div class="top-link" id="top-link">
                                    <div class="select-lang"><a href="{{ route('login' )}}" title="Home">Log in</a></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header">
                    <div class="container">
                        <div class="header-content">
                            
                            <div class="button">
                                <a href="#">
                                    <div class="button"><b></b></button>
                                </a>
                            </div>
                        </div>
                        <div class="quick-access">
                           <form class="form-inline my-2 my-lg-0"  style=" float: right;" method="GET" role="search" action="{{route('search.create')}}">
                    @csrf
                    <input class="form-control mr-sm-2" placeholder="Search" name="search" aria-label="Search" style="margin-top: 5px;">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" style="margin-top: 5px;">Search</button>
                  </form> 
                            <div class="top-cart-wrapper">
                                <div class="top-cart-contain">
                                    
                                    <div id="mini_cart_block">
                                        <div class="block-cart mini_cart_ajax">
                                            <div class="block-cart">
                                                <div class="top-cart-title">
                                                    <a href="{{ route('cart.index') }}">Shopping Cart
                                                        <span class="badge">{{ Cart::count()}}</span>
                                                    </a>
                                                </div>
                                                <div class="top-cart-content">
                                                    <p class="empty">You have no items in your shopping cart.</p>
                                                    <div class="top-subtotal">Subtotal: <span class="price">€0.00</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
    <div class="row">
        <div class="col-xs-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-xs-6">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                            </div>
                            <div class="col-xs-6">
                                <a href="/"><button type="button" class="btn btn-primary btn-sm btn-block">
                                    <span class="glyphicon glyphicon-share-alt"></span> Continue shopping
                                </button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @if(Cart::count() > 0)
                    @foreach($carts as $cart)
                    <div class="row">
                        
                        <div class="col-xs-2"><img class="img-responsive" src="{{asset('storage/' . $cart->options->image)}}">
                        </div>
                        <div class="col-xs-4">
                            <h4 class="product-name"><strong>{{ $cart->name }}</strong></h4><h5>Price: {{ $cart->price }}$</h5>
                        </div>
                        <div class="col-xs-6">
                            <div class="col-xs-6 text-right">
                                
                            </div>
                            <div class="col-xs-4">
                                
                            </div>
                            <div class="col-xs-2">
                                
                    <form style="float: right; vertical-align: middle;" method="POST" action="{{ route('cart.destroy' , $cart->rowId) }}">
                                            @csrf
                                             @method('DELETE')
                                   <button type="submit" class="btn btn-link btn-xs">

                                    <span style="font-size: 20px;" class="glyphicon glyphicon-trash"> </span>
                                </button> </form>
                                
                            </div>
                            <h5>Vendos Sasine:</h5>
                            <form action="{{ route('cart.update' , $cart->rowId) }}" method="Post">
                                        @csrf
                                        @method('PUT')
                                                <input type="number" name="qty" value="{{$cart->qty}}">                     
                                    <button type="submit" class="btn btn-info" value="Ok">OK</button>
                                    </form> 
                                    
                        </div>
                    </div>
                    <hr>
                    
                    @endforeach
                     @else
            <div class="row">
                <div class="col-sm-6 col-md-offset-3 col-sm-offset-3">
                    <h2>No Articles in the cart</h2>
                </div>
            </div>
        @endif
                    
                    
                </div>
                <div class="panel-footer">
                    <div class="row text-center">
                        <div class="col-xs-9">
                            <h4 class="text-right">Total <strong>${{Cart::subtotal()}}</strong></h4>
                        </div>
                        <div class="col-xs-3">
                            <a href="{{ route('orders.create')}}"><button type="button" class="btn btn-success btn-block">
                                Checkout
                            </button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        </body>
        









</html>
