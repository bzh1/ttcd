<!DOCTYPE html>
<style>
    .headero {
  padding: 20px;
  text-align: center;
  background: grey;
  color: white;
}
.footer {
  padding: 20px;
  text-align: center;
  background: grey;
  }

    </style>
<html lang="de-CH" class="no-js">

<head>
    <title>KOLICA</title>
    <link rel="stylesheet" href="{{asset('css\app.css')}}">
    <link rel="stylesheet" href="{{asset('css\icons.css')}}">
    <meta name="robots" content="all,noodp">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="application-name" content="Elektrogrosshandel">
    <meta name="theme-color" content="#0083C3">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="k8E543y1c8pfewXIGW0kjeSwVS8ejVvAU6aiuCRSCiic0uXQcyvO8sXUDcXYhrAC">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/static/images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/static/images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-128.png" sizes="128x128">
    <meta name="msapplication-config" content="/static/ieconfig.xml">
    <link rel="manifest" href="/static/manifest.webmanifest">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=greek">
    <link rel="stylesheet" type="text/css" href="/static/css/icons.css?f4a2080209ffae2cdba0e">
    <link rel="stylesheet" type="text/css" href="/static/css/app.css?f4a2080209ffae2cdba0e">
    <script>
        var cssLoaded = true;
            </script>
    <script defer src="/static/js/app.js?f4a2080209ffae2cdba0e"></script>
    <link rel="canonical" href="https://www.elektrogrosshandel.ch/de/costcenters/">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53269130-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                        gtag('config', 'UA-53269130-1');
                        
                    </script>
</head>

<header class="shopheader">
    <section class="shopheader__middle">
        </a>
        </div>
        <div class="shopheader__search">
            <div class="shopheader__search-instant-results" data-instant-search-results></div>
        </div>
        <section class="shopheader__account">
            <a href="{{ url('/')}}"><i class="icon icon-cart medium"></i>Shopi</a>
            <a href="{{ url('/home') }}" data-toggle="account-menu" title="Mein Konto">
                <i class="icon icon-user medium"></i>
                Kolica
            </a>
            
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out" style="font-size:20px;color:red"></i>Log Out</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
            
            <div class="shopheader-menu shopheader-menu--account dropdown-pane widget" id="account-menu" data-dropdown data-hover="true" data-hover-pane="true" data-position="bottom" data-alignment="right">
                <div class="widget__heading">
                    <form method="POST" class="width-100" action="/de/account/logout/">
                        <input type="hidden" name="csrfmiddlewaretoken" value="QAtYp5OejKarMVrbXLYOQpQwNCxTCyugqyZbPE75JU3oyTrjtntiFDVUvWmDA4zi">
                        <button type="submit" class="button hollow margin-top-1 width-100">
                            <i class="icon icon-logout medium"></i>
                            <span>Abmelden</span>
                        </button>
                    </form>
                </div>
            </div>
            <a href="{{ route('cart.index') }}" title=" " data-toggle="cart-menu" data-bind-prop-set="cart:activeCartLabel,title=Warenkorb {}">
                <i class="icon icon-cart medium"></i>  Shoping Card
                <a href="html.html">
                    <span class="badge hide" data-bind-text="cart:activePositionCount" data-bind-class="cart:isActiveEmpty,hide">
                    </span>
                </a>
                <div class="shopheader-menu shopheader-menu--cart dropdown-pane widget " data-bind-prop-set="cart:hasActivePositions,data-cart-preview=/de/cart/preview" data-cart-preview=/de/cart/preview id="cart-menu" data-dropdown data-hover="true" data-hover-pane="true" data-position="bottom" data-alignment="right">
                    <div class="cart-preview" data-cart-preview-widget>
                        <div class="cart-preview__loader"><i class="icon loader"></i></div>
                        <div class="cart-preview__content" data-bind-innerhtml="cart:preview"></div>
                    </div>
                </div>
        </section>
        </div>
    </section>
    </div>
    </div>
    </div>
    </div>
    </nav>
</header>
<h1 style="text-align:center;">
    Llogaria ime
</h1>
</div>
</header>
<section class="container padding-bottom-3 padding-top-1">
    <div class="grid-x grid-margin-x grid-margin-y" style="justify-content: center;">
        <div class="widget cell small-12 laptop-8 desktop-6 shadow">
            <div class="widget__heading">
                <h5 class="widget__title flex-child-grow">
                    <i class="icon icon-gear"></i>
                    <span>Menaxhimi</span>
                </h5>
            </div>
            <div class="widget__content">
                <div class="grid-x grid-margin-x phablet-up-2">
                    <a href="{{route('commission.index')}}" class="cell margin-bottom-1">
                        <i class="icon icon-list primary medium"></i>
                        <span class="black">
                            Kommissionen
                        </span>
                    </a>
                    <a href="/projekt" class="cell margin-bottom-1">
                        <i class="icon icon-shoppinglist primary medium"></i>
                        <span class="black">
                            Editori
                        </span>
                    </a>
                    <a href="{{ route('cart.index') }}" class="cell margin-bottom-1">
                        <i class="icon icon-carts primary medium"></i>
                        <span class="black">
                            Karrocat e blerjeve
                        </span>
                    </a>
                    
                    <a href="{{ route('offers.index') }}" class="cell margin-bottom-1">
                        <i class="icon icon-offer-doc primary medium"></i>
                        <span class="black">
                            Ofertat
                        </span>
                    </a>
                    <a href="{{route('orders.index')}}" class="cell margin-bottom-1">
                        <i class="icon icon-backorder primary medium"></i>
                        <span class="black">
                            Porosite
                        </span>
                    </a>
                    <a href="{{route('objekt.index')}}" class="cell margin-bottom-1">
                        <i class="icon icon-object primary medium"></i>
                        <span class="black">
                            Menaxhimi i pasurisë
                        </span>
                    </a>
                    <a href="{{ url('/') }}" class="cell margin-bottom-1">
                        <i class="icon icon-object primary medium"></i>
                        <span class="black">
                            Shopi
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="widget cell small-12 laptop-8 desktop-6 shadow">
            <div class="widget__heading">
                <h5 class="widget__title flex-child-grow">
                    <i class="icon icon-user"></i>
                    <span>
                        Të dhënat e përdoruesit dhe kompanisë
                    </span>
                </h5>
            </div>
            <div class="widget__content">
                <div class="grid-x grid-margin-x grid-margin-y phablet-up-2">
                    <div class="cell">
                        <h6 class="widget__subtitle">Nr. i perdoruesit: {{Auth::user()->nr}}</h6>
                        <strong></strong>
                    </div>
                    <div class="cell">
                        <h6 class="widget__subtitle">Perdoruesi: {{Auth::user()->name}}</h6>
                        <strong></strong>
                    </div>
                    <div class="cell">
                        <h6 class="widget__subtitle">Emri i perdoruesit: {{Auth::user()->emri}}</h6>
                        <strong></strong>
                    </div>
                    <div class="cell">
                        <h6 class="widget__subtitle">Roli i autorizimit: Perdorues</h6>
                        <strong></strong>
                    </div>
                    <div class="cell">
                        <h6 class="widget__subtitle">
                            Firma: {{Auth::user()->firma}}
                    </div>
                    <div class="cell">
                        <h6 class="widget__subtitle">E-mail: {{Auth::user()->email,}}</h6>
                        <strong></strong>
                    </div>
                </div>
            </div>
            <div class="widget__footer grid-x grid-margin-y grid-margin-x align-right">
                <div class="cell phablet-shrink phablet-order-2">
                    <a href="{{route('user.edit')}}" class="button phone-down-expanded">
                        <span>redaktoj</span>
                        <i class="icon icon-edit"></i>
                    </a>
                </div>
             
            </div>
        </div>
        <div class="widget cell small-12 laptop-8 desktop-6 shadow">
            <div class="widget__heading">
                <h5 class="widget__title">
                    <i class="icon icon-cart"></i>
                    <span>
                        3 Porositë e fundit</span>
                </h5>
            </div>
            <div class="widget__content">
                <div class="table-scroll">
                    <table class="rows width-100">
                        <thead>
                            <tr>
                                <th data-ordering="name" data-searchable class="">Id</th>
                                <th data-ordering="name" data-searchable class="">Emri i artiku-llit/jve</th>
                                <th data-ordering="comment" data-searchableclass="sortable show-for-tablet">Emri i Perdoruesit</th>
                                <th>Totali</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->article_name}}</td>
                                <td>{{$order->user_name}}</td>
                                <td>{{$order->price}}$</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget__footer">
                <a href="{{route('orders.index')}}" class="button float-right margin-top-1 phone-down-expanded">
                    <span></span>
                    <i class="icon icon-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</section>
</main>

</body>

</html>

