@extends('layouts.app')

@section('content')


<style type="text/css">
    .header {
        display: none;
    }
    .footer {
        display: none;
    }
</style>
<div  class="padding-vertical-3 bg-lightest-gray">
    <div class="registration container tiny">
        <div class="registration__box">
            <div class="card">
                <h1 style="text-align:center;">Login</h1>

                <div class="animate form">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Your email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="valon123 ose valon123@mail.com" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Your password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                {{-- <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Keep me logged in') }}
                                    </label>
                                </div> --}}
                            </div>
                        </div>

                        <div class="form__footer form__footer--borderless">
                            <div class="form__footer__col">
                                <button type="submit" style="width: 95px;" class="button expanded ">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                                <div class="text-right padding-top-1">
                                    <a href="/register">Register</a>
                                

                                <div class="text-right padding-top-1">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>

                                @endif
                            </div>

                            </div>
                        <br> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
