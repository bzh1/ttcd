<!DOCTYPE html>

<style>
.headero {
  padding: 20px;
  text-align: center;
  background: grey;
  color: white;
}
.footer {
  padding: 20px;
  text-align: center;
  background: grey;
  }

    </style>
        <html lang="de-CH" class="no-js">
 
    <head>
        <title>KOMMISONI</title>
        <link rel="stylesheet" href="{{asset('css\app.css')}}">
        <link rel="stylesheet" href="{{asset('css\icons.css')}}">
       <meta name="robots" content="all,noodp">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="application-name" content="Elektrogrosshandel">
            <meta name="theme-color" content="#0083C3">
            <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="csrf-token" content="k8E543y1c8pfewXIGW0kjeSwVS8ejVvAU6aiuCRSCiic0uXQcyvO8sXUDcXYhrAC">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/static/images/favicon/apple-touch-icon-57x57.png">
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/images/favicon/apple-touch-icon-114x114.png">
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/images/favicon/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/images/favicon/apple-touch-icon-144x144.png">
            <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/static/images/favicon/apple-touch-icon-60x60.png">
            <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/images/favicon/apple-touch-icon-120x120.png">
            <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/images/favicon/apple-touch-icon-76x76.png">
            <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/images/favicon/apple-touch-icon-152x152.png">
            <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/favicon.ico">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-196x196.png" sizes="196x196">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-96x96.png" sizes="96x96">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-32x32.png" sizes="32x32">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-16x16.png" sizes="16x16">
            <link rel="icon" type="image/png" href="/static/images/favicon/favicon-128.png" sizes="128x128">   
    <meta name="msapplication-config" content="/static/ieconfig.xml">
    <link rel="manifest" href="/static/manifest.webmanifest"> 
            <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=greek">    
            <link rel="stylesheet" type="text/css" href="/static/css/icons.css?f4a2080209ffae2cdba0e">
            <link rel="stylesheet" type="text/css" href="/static/css/app.css?f4a2080209ffae2cdba0e">      
            <script>              
                var cssLoaded = true;
            </script>
            <script defer src="/static/js/app.js?f4a2080209ffae2cdba0e"></script>
            <link rel="canonical" href="https://www.elektrogrosshandel.ch/de/costcenters/">               
                        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53269130-1"></script>                
                    <script>
                        window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                        gtag('config', 'UA-53269130-1');
                        
                    </script> 
    </head>


        <header class="print-header grid-x">
    
        <div class="cell small-5">
            <img src="/static/images/logo.svg" class="print-header__logo">
        </div>
    
    
        <div class="cell small-7">
            <div class="grid-x grid-margin-x">
                <div class="cell auto">
                  
                </div>
                   </div>
            </div>
        </div>
    
</header>


        <header class="print-header grid-x">
    
        <div class="cell small-5">
            <img src="/static/images/logo.svg" class="print-header__logo">
        </div>
    
    
        <div class="cell small-7">
            <div class="grid-x grid-margin-x">
                <div class="cell auto">
                    
                        
                            &lt;div class=&quot;grid-x grid-margin-x grid-margin-y&quot;&gt;<br>&lt;div class=&quot;cell small-12 tablet-4&quot;&gt;<br>&lt;h4&gt;A. Saesseli &amp; Co. AG&lt;/h4&gt;<br>Pflanzschulstrasse 17&lt;br&gt;<br>8400 Winterthur&lt;br&gt;<br>&lt;br&gt;<br>Tel. &lt;a href=&quot;tel:+41522352626&quot;&gt;+41 52 235 26 26&lt;/a&gt;&lt;br&gt;<br>&lt;a href=&quot;mailto:verkauf@saesseli.ch&quot;&gt;verkauf@saesseli.ch&lt;/a&gt;<br>&lt;/div&gt;<br>&lt;div class=&quot;cell small-12 tablet-4&quot;&gt;<br>&lt;h4&gt;EL Kabel AG&lt;/h4&gt;<br>Leisibachstrasse 9&lt;br&gt;<br>6307 Root&lt;br&gt;<br>&lt;br&gt;<br>Tel. &lt;a href=&quot;tel:+41414555070&quot;&gt;+41 41 455 50 70&lt;/a&gt;&lt;br&gt;<br>&lt;a href=&quot;mailto:verkauf@elkabel.ch&quot;&gt;verkauf@elkabel.ch&lt;/a&gt;<br>&lt;/div&gt;<br>&lt;div class=&quot;cell small-12 tablet-4&quot;&gt;<br>&lt;h4&gt;Standard AG&lt;/h4&gt;<br>Freulerstrasse 6&lt;br&gt;<br>4127 Birsfelden&lt;br&gt;<br>&lt;br&gt;<br>Tel. &lt;a href=&quot;tel:+41613788200&quot;&gt;+41 61 378 82 00&lt;/a&gt;&lt;br&gt;<br>&lt;a href=&quot;mailto:verkauf@standard.ch&quot;&gt;verkauf@standard.ch&lt;/a&gt;<br>&lt;/div&gt;<br>&lt;/div&gt;
                        
                    
                </div>
                <div class="cell auto">
                    
                        
                            info@elektrogrosshandel.ch<br>
                        
                        
                    
                </div>
            </div>
        </div>
    
</header>

    

    
        <main id="app" class="">                
            
        </div>
<div class="container medium padding-vertical-3">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div>
                

                <div class="container medium padding-vertical-3">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                            <h1> Sign up </h1>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Your name') }}</label>

                            <div class="col-md-6">
                                <input class="uname" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Valon" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Your email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="valon123@gmail.com" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Your password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Please confirm your password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>





                        <div class="form-group row">
                            <label for="firma" class="col-md-4 col-form-label text-md-right">{{ __('Firma') }}</label>

                            <div class="col-md-6">
                                <input @error('firma') is-invalid @enderror id="firma" type="text" name="firma"  required autocomplete="firma" value="{{ old('firma') }}" autofocus>

                                @error('firma')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>





                         <div class="form-group row">
                            <label for="rruga" class="col-md-4 col-form-label text-md-right">{{ __('Rruga') }}</label>

                            <div class="col-md-6">
                                <input @error('rruga') is-invalid @enderror id="rruga" type="text" name="rruga"  required autocomplete="rruga" value="{{ old('rruga') }}" autofocus>
                                @error('rruga')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="nr" class="col-md-4 col-form-label text-md-right">{{ __('Nr i identifikimit') }}</label>

                            <div class="col-md-6">
                                <input @error('nr') is-invalid @enderror id="nr" type="text" name="nr"  required autocomplete="nr" value="{{ old('nr') }}" autofocus>
                                @error('nr')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 



 <!--                        <div class="form-group row">
                            <label for="emailfirma" class="col-md-4 col-form-label text-md-right">{{ __('Email firma') }}</label>

                            <div class="col-md-6">
                                <input id="emailfirma" type="text" name="emailfirma"  required autocomplete="emailfirma" autofocus>
                            </div>
                        </div> -->

                         <div class="form-group row">
                            <label for="telefon" class="col-md-4 col-form-label text-md-right">{{ __('Telefon') }}</label>

                            <div class="col-md-6">
                                <input @error('telefon') is-invalid @enderror id="telefon" type="text" name="telefon"  required autocomplete="telefon" value="{{ old('telefon') }}" autofocus>
                                @error('telefon')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="biznesi" class="col-md-4 col-form-label text-md-right">{{ __('Biznesi') }}</label>

                            <div class="col-md-6">
                                <input @error('biznesi') is-invalid @enderror id="biznesi" type="text" name="biznesi"  required autocomplete="biznesi" value="{{ old('biznesi') }}" autofocus>
                                @error('biznesi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="emri" class="col-md-4 col-form-label text-md-right">{{ __('Emri') }}</label>

                            <div class="col-md-6">
                                <input @error('emri') is-invalid @enderror id="emri" type="text" name="emri"  required autocomplete="emri" value="{{ old('emri') }}" autofocus>
                                @error('emri')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mbiemri" class="col-md-4 col-form-label text-md-right">{{ __('mbiemër') }}</label>

                            <div class="col-md-6">
                                <input @error('mbiemri') is-invalid @enderror id="mbiemri" type="text" name="mbiemri"  required autocomplete="mbiemri" value="{{ old('mbiemri') }}" autofocus>
                                @error('mbiemri')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="emailp" class="col-md-4 col-form-label text-md-right">{{ __('E-mail personal') }}</label>

                            <div class="col-md-6">
                                <input @error('emailp') is-invalid @enderror id="emailp" type="text" name="emailp"  required autocomplete="emailp" value="{{ old('emailp') }}" autofocus>
                                @error('emailp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>







                        <div class="form__footer form__footer--borderless">
                            <div class="form__footer__col">
                                <button type="submit" class="button expanded ">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


        </main>                                
 
    </body>
</html>