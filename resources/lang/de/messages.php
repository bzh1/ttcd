<?php

return [

    'dashboard' => 'Instrumententafel',
    'hause' => 'FUR ZU HAUSE',
    'buro' => 'FUR DAS BURO',
    'baus' => 'FUR DIE BAUSTELLE',
    'login' => 'Anmeldung',
    'logout' => 'Ausloggen',
    'createArticle' => 'ARTIKEL ERSTELLEN',
    'home' => 'ZUHAUSE',

];
