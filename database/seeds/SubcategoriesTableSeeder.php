<?php

use Illuminate\Database\Seeder;

class SubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();
// Category 1
        DB::table('subcategories')->insert([
            'name' => 'Dechkenleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '1',
            'category_id' => '1',
        ]);
//
         DB::table('subcategories')->insert([
            'name' => 'Wandleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '2',
            'category_id' => '1',
        ]);
//
         DB::table('subcategories')->insert([
            'name' => 'LED-Bander',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '3',
            'category_id' => '1',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'Spots',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '4',
            'category_id' => '1',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'Gartenleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '5',
            'category_id' => '1',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'Leuchtmittel/ Netzteile',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '6',
            'category_id' => '1',
        ]);
         DB::table('subcategories')->insert([
            'name' => 'Zubehor',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '7',
            'category_id' => '1',
        ]);



 // Category 2

          DB::table('subcategories')->insert([
            'name' => 'Dechkenleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '8',
            'category_id' => '2',
        ]);
//
         DB::table('subcategories')->insert([
            'name' => 'Wandleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '9',
            'category_id' => '2',
        ]);
//
         DB::table('subcategories')->insert([
            'name' => 'Stehleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '10',
            'category_id' => '2',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'Spots',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '11',
            'category_id' => '2',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'Tischleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '12',
            'category_id' => '2',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'Schienensystem',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '13',
            'category_id' => '2',
        ]);
         DB::table('subcategories')->insert([
            'name' => 'Leuchtmittel/ Netzteile',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '14',
            'category_id' => '2',
        ]);
         DB::table('subcategories')->insert([
            'name' => 'Zubehor',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '15',
            'category_id' => '2',
        ]);


        // Category 3

          DB::table('subcategories')->insert([
            'name' => 'Dechkenleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '16',
            'category_id' => '3',
        ]);
//
         DB::table('subcategories')->insert([
            'name' => 'Wandleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '17',
            'category_id' => '3',
        ]);
//
         DB::table('subcategories')->insert([
            'name' => 'Spots',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '18',
            'category_id' => '3',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'Balkenleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '19',
            'category_id' => '3',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'Schienensystem',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '20',
            'category_id' => '3',
        ]);
 //
         DB::table('subcategories')->insert([
            'name' => 'LED-Bander / Profile',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '21',
            'category_id' => '3',
        ]);
         DB::table('subcategories')->insert([
            'name' => 'Exit-Leuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '22',
            'category_id' => '3',
        ]);
         DB::table('subcategories')->insert([
            'name' => 'Gartenleuchten',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '23',
            'category_id' => '3',
        ]);
         DB::table('subcategories')->insert([
            'name' => 'IP 65 & 68',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '24',
            'category_id' => '3',
        ]);
             DB::table('subcategories')->insert([
            'name' => 'Leuchtmittel/ Netzteile',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '25',
            'category_id' => '3',
        ]);
         DB::table('subcategories')->insert([
            'name' => 'Zubehor',
            'image'=>$faker->image(public_path() . '\storage' ,800, 600, [], []),
        ]);
        DB::table('categories_subcategories')->insert([
            'subcategory_id' => '26',
            'category_id' => '3',
        ]);
    }
}
