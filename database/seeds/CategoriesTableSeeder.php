<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'hause',
        ]);
        DB::table('categories')->insert([
            'name' => 'buros',
        ]);
        DB::table('categories')->insert([
            'name' => 'baus',
        ]);
    }
}
