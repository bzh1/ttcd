<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'firma' => 'firma',
            'rruga' => 'rruga',
            'nr' =>	'1234',
            'isadmin' => true,
            'telefon' => '1234',
            'biznesi' => 'biznesi', 
            'emri' => 'emri',
            'mbiemri' => 'mbiemri',
            'emailp' => 'emailprivat@gmail.com',
        ]);
    }
}
