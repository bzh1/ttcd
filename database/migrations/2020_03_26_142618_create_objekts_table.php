<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjektsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objekts', function (Blueprint $table) {
            $table->id();
            $table->string('objekt')->nullable();
            $table->string('name2')->nullable();
            $table->string('kostoja');
            $table->string('ort');
            $table->string('adresa')->nullable();
            $table->string('firma');
            $table->string('rruga');
            $table->string('rruga2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objekts');
    }
}
