<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use ChristianKuri\LaravelFavorite\Traits\Favoriteability;

class User extends Authenticatable implements MustVerifyEmail
{ 
    use Notifiable;
    use Favoriteability;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'firma','rruga','nr','telefon','biznesi','emri','mbiemri','emailp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

        public function commissions()
        {
            return $this->hasMany('App\Models\Commission');
        }
        public function objects(){
            return $this->hasMany('App\Models\Objekt');
        } 
        public function orders(){
            return $this->hasMany('App\Models\Order');
        } 
        public function offers(){
            return $this->hasMany('App\Models\Offer');
        }
}
