<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = ['name'];
	
    public function article()
    {
    	return $this->belongsToMany('App\Models\Article' , 'categories_articles' , 'category_id' , 'article_id');
    }
     
     public function subcategory()
    {
    	return $this->belongsToMany('App\Models\Subcategory' , 'categories_subcategories' ,'category_id' , 'subcategory_id'); 
    }
}
