<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;

class Article extends Model
{
    use Favoriteable;
    
     protected $fillable = ['name','company','desc','price','image'];

     protected $table = 'article';

      public function category()
    {
    	return $this->belongsToMany('App\Models\Category' , 'categories_articles' , 'article_id' , 'category_id');
    }
     
     public function subcategory()
    {
    	return $this->belongsToMany('App\Models\Subcategory' , 'subcategories_articles' ,'article_id' ,'subcategory_id');
    }

    public function offer()
    {
        return $this->belongsToMany('App\Models\Offer' ,'articles_offers' , 'article_id' , 'offer_id'); 
    }
} 
    

