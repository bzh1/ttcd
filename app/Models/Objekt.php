<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Objekt extends Model
{
    Protected $fillable = ['objekt','name2','kostoja','ort','adresa','firma','rruga','rruga2'];
    
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
