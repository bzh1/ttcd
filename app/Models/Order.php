<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['article_name' ,'price' , 'address', 'qty' , 'telefon' , 'user_name' , 'user_id' ];

     public function user()
    {
    	return $this->belongsTo('App\User' , 'user_id');
    }
}
