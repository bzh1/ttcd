<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Commission extends Model
{
    protected $fillable = ['name','desc', 'user_id'];

    public function user()
    {
    	return $this->belongsTo('App\User' , 'user_id');
    }
}
