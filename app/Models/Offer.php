<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = ['name' , 'user_id' , 'email' , 'qty' , 'price'];

    public function user()
    {
    	return $this->belongsTo('App\User' , 'user_id');
    }

    public function article(){
            return $this->belongsToMany('App\Models\Article' , 'articles_offers' , 'offer_id' , 'article_id');
        }
}
