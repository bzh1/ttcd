<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = ['name' , 'image'];

      public function category()
    {
    	return $this->belongsToMany('App\Models\Category' , 'categories_subcategories' ,'subcategory_id' , 'category_id');
    }
    public function article()
    {
    	return $this->belongsToMany('App\Models\Article' , 'subcategories_articles' , 'subcategory_id' ,'article_id'); 
    }

}
