<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Subcategory;

class BauController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $categories = Category::where('id','3')->get();
       foreach ($categories as $category) {
         $articles = $category->article()->paginate(12);  
         $subcategories = $category->subcategory()->take(6)->get();  
       }
        return view('baus.index', compact(['articles' , 'categories', 'subcategories']));

    }

     public function show($id)
    {
        $articles = Article::find($id);
        return view('baus.show')->with('article',$articles);
    }


  }