<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Objekt;
use App\Http\Requests\ObjektStore;
use App\Http\Requests\ObjektUpdate;
use App\User;

class ObjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Objekt $objects)
    {
        $user_id = auth()->user()->id;
        $objects = User::find($user_id)->objects;    
        return view('objekt.Objekte', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('objekt.Objekt_erstellen');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ObjektStore $request)
    {
         $object = new Objekt;
         $object = Objekt::create($request->all());
         $object->user_id = auth()->user()->id;
         $object->save();
         return redirect()->route('objekt.index')->with('status','Commission created sucesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $objekt = Objekt::find($id);
        return view('objekt.edit')->with('objekt', $objekt);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ObjektUpdate $request, $id)
    {
        $data = $request->all();
        Objekt::find($id)->update($data);
        return redirect()->route('objekt.index')->with('warning','Komisioni u ndryshua ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objekt =Objekt::find($id);
         $objekt->delete();
         return redirect()->route('objekt.index')->with('danger','Komisioni u fshi');
    }
}
