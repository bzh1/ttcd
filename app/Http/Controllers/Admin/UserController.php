<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => ['required', 'string', 'max:255' ],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8' , 'confirmed'],
            'firma' =>['required', 'string', 'max:255'],
            'rruga' =>['required', 'string', 'max:255'],
            'nr'  =>['required','regex:/[0-9]/'],
            'telefon' =>['required','regex:/[0-9]/'],
            'biznesi' =>['required', 'string', 'max:255'],
            'emri' =>['required', 'string', 'max:255'],
            'mbiemri' =>['required', 'string', 'max:255'],
            'emailp' =>['required', 'string', 'email', 'max:255',],
            ]);

            $user = new User;
            $user->name = $request['name'];
            $user->password = bcrypt(request('password'));
            $user->email = $request['email'];
            $user->firma = $request['firma'];
            $user->rruga = $request['rruga'];
            $user->nr = $request['nr'];
            $user->telefon = $request['telefon'];
            $user->biznesi = $request['biznesi'];
            $user->emri = $request['emri'];
            $user->mbiemri = $request['mbiemri'];
            $user->emailp = $request['emailp'];
            $user->email_verified_at = now();
            $user->save();

            return redirect()->back()->with('success', 'user created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
