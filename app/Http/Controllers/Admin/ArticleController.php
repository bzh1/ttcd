<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleStore;
use App\Http\Requests\ArticleUpdate;
use App\Models\Article;
use App\Models\Category;
use App\Models\Subcategory;
use File;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Article $articles)
    {
         $articles = Article::all();
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        return view('admin.articles.create')->with(['categories'=>$categories , 'subcategories'=>$subcategories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleStore $request)
    {     
         // $i = 0;
         // foreach ($request->category_id as $category) {
         $article =Article::create($request->all());
         $article->category()->attach($request->category_id);
         $article->subcategory()->attach($request->subcategory_id);
         $this->storeImage($article);
         // $i = $i +1;
         // }

        return redirect()->route('hause.index')->with('status','Article created sucesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $article = Article::find($id);
        return view('admin.articles.show')->with('article',$article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        return view('admin.articles.edit')->with('article', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleUpdate $request, $id)
    {
        $data = $request->all();
        Article::find($id)->update($data);
        return redirect()->route('adminarticles.index')->with('warning','Artikulli u ndryshua ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   $article =Article::find($id);
        if ($article != null) {
         if(isset($article->image)){
           $img = $article->image;
           $filename =public_path().'/storage/'.$img;
            File::delete($filename);
         }  
        $article->delete();
        }
        return redirect()->route('adminarticles.index')->with('danger','Komisioni u fshi');
    }


    private function storeImage($customer){
        if(request()->has('image')){
            $customer->update([
                'image' => request()->image->store('uploads', 'public')
            ]);
        }
    }
}