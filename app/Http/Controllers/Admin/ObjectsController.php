<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ObjektStore;
use App\Http\Requests\ObjektUpdate;
use App\Models\Objekt;

class ObjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Objekt $objects)
    {   
        $objects = Objekt::all(); 
        return view('admin.objekt.Objekte', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.objekt.Objekt_erstellen');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ObjektStore $request)
    {
         Objekt::create($request->all());
        return redirect()->route('adminobjekt.index')->with('status','Object created sucesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $objekt = Objekt::find($id);
        return view('admin.objekt.edit')->with('objekt', $objekt);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ObjektUpdate $request, $id)
    {
        $data = $request->all();
        Objekt::find($id)->update($data);
        return redirect()->route('adminobjekt.index')->with('warning','Objekti u ndryshua ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objekt =Objekt::find($id);
         $objekt->delete();
         return redirect()->route('adminobjekt.index')->with('danger','Objekti u fshi');
    }
}
