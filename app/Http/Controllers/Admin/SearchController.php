<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Category;
use App\Models\Subcategory;

class SearchController extends Controller
{
    public function search()
    {
    	$categories = Category::all();
        $subcategories = Subcategory::all();
        $search = \Request::get('search');
        $articles = Article::where('name' , 'like' , '%' .$search. '%')->orWhere('desc' , 'like' , '%' .$search. '%')->orderBy('id')->paginate(8);
        $articles->appends(['search' => $search]);
        return view('admin.search' , compact(['search' , 'articles' , 'categories' , 'subcategories']));
    }
}
