<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Article;
use App\Models\Offer;
use App\Http\Requests\OfferStore;


class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offer::all();

        return view('admin.offers.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $articles = Article::all();
        $users = User::all();
        return view('admin.offers.create' , compact(['users', 'articles']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfferStore $request)
    {    
        $i = 0;
        foreach ($request->article_id as $articles) {
        $offer = new Offer;
        $offer->name = $request['name'];
        $offer->email = $request['email'];
        $offer->user_id = $request['user_id'];
        $offer->qty = $request['qty'];  
        //Offer discount
        $selectedArticle = Article::find($articles);
        $price = $selectedArticle->price/100;
        $percentage = $price * $request['price'];
        $offer->price = $selectedArticle->price - $percentage;
        $offer->save();
        $offer->article()->attach($request->article_id[$i]);
        $i= $i +1;


        }
        //offer discount end
        return redirect()->route('adminoffers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $articles = Article::all();
        $users = User::all();
        $offer = Offer::find($id);
        return view('admin.offers.edit' , compact(['articles' , 'users' , 'offer']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
             'name' => 'required',
            'user_id' => 'required',
            'email' => 'required',
            'qty' => 'required',
            'price' => 'required|numeric',
                ]);
        $data = $request->all();
        Offer::find($id)->update($data);
        return redirect()->route('adminoffers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offers = Offer::find($id);
        $offers->delete();
        return redirect()->back();

    }
}
