<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CommissionStore;
use App\Http\Requests\CommissionUpdate;
use App\Models\Commission;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Commission $commissions)
    {    
        // return view('commission.komisioni');
        $commissions = Commission::all();
        return view('admin.commission.komisioni', compact('commissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.commission.Kommission_erstellen');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommissionStore $request)
    {
        Commission::create($request->all());
        return redirect()->route('admincommission.index')->with('status','Commission created sucesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admincommission.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $commission = Commission::find($id);
        return view('admin.commission.edit')->with('commission', $commission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommissionUpdate $request, $id)
    {
        $data = $request->all();
        Commission::find($id)->update($data);
        return redirect()->route('admincommission.index')->with('warning','Komisioni u ndryshua ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $commissions =Commission::find($id);
         $commissions->delete();
         return redirect()->route('admincommission.index')->with('danger','Komisioni u fshi');
    }
}


