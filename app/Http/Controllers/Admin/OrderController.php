<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Http\Requests\OrderUpdate;
use PDF;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();  
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
       /* $orderPDF = $order->id.'. Article Name:'.$order->article_name.'- User Name:'.$order->user_name.'- Quantity:'.$order->qty.'- Price:'.$order->price.'- User Phone:'.$order->telefon;*/
          $orderPDF = '
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" integrity="sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd" crossorigin="anonymous"></script>
        <table class="table" data-table="costcenter-list" data-highlight data-sortable-table="kommission_erstellen">
            <thead>
             <tr>                     
                    <th>Id</th>                      
                    <th>Emri i artiku-llit :</th>                      
                    <th>Emri i Perdoruesit: </th>
                    <th>Nr. i telefonit :</th>
                    <th>Sasi-a/te :</th>
                    <th>Adresa :</th>
                    <th>Totali :</th>
                 </tr>
            </thead>
            <tbody>
            <tr>
            <td>'.$order->id.'</td>
            <td>'.$order->article_name.'</td>
            <td>'.$order->user_name.'</td>
            <td>'.$order->telefon.'</td>
            <td>'.$order->qty.'</td>
            <td>'.$order->address.'</td>
            <td>'.$order->price.'</td>
            </tr>';
        $pdf = PDF::loadHTML($orderPDF);
        return $pdf->download('order.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $order = Order::find($id);
        return view('admin.orders.edit')->with('order', $order);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderUpdate $request, $id)
    {
         $data = $request->all();
        Order::find($id)->update($data);
        return redirect()->route('adminorders.index')->with('warning','Komisioni u ndryshua ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders =Order::find($id);
         $orders->delete();
         return redirect()->route('adminorders.index')->with('danger','Orderi u fshi');
    }
}
