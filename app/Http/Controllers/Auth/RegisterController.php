<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::VERIFY;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255' ],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'firma' =>['required', 'string', 'max:255'],
            'rruga' =>['required', 'string', 'max:255'],
            'nr'  =>['required','regex:/[0-9]/'],
            'telefon' =>['required','regex:/[0-9]/'],
            'biznesi' =>['required', 'string', 'max:255'],
            'emri' =>['required', 'string', 'max:255'],
            'mbiemri' =>['required', 'string', 'max:255'],
            'emailp' =>['required', 'string', 'email', 'max:255',],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'firma' => $data['firma'],
            'rruga' => $data['rruga'],
            'nr' => $data['nr'],
            'telefon' => $data['telefon'],
            'biznesi' => $data['biznesi'],
            'emri' => $data['emri'],
            'mbiemri' => $data['mbiemri'],
            'emailp' => $data['emailp'],
            'password' => Hash::make($data['password']),
        ]); 
        $user = 'admin@admin.com';
        Mail::to($user)->send(new VerifyEmail($user));
    }
}
