<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use Illuminate\Http\Request;
use App\Http\Requests\CommissionStore;
use App\Http\Requests\CommissionUpdate;
use App\User;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        // return view('commission.komisioni');
        $user_id = auth()->user()->id;
        $commissions = User::find($user_id)->commissions;
        return view('commission.komisioni', compact('commissions'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('commission.Kommission_erstellen');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommissionStore $request)
    {
        $commission = new Commission;
        $commission = Commission::create($request->all());
        $commission->user_id = auth()->user()->id;
        $commission->save();
        return redirect()->route('commission.index')->with('status','Commission created sucesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('commission.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $commission = Commission::find($id);
        return view('commission.edit')->with('commission', $commission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommissionUpdate $request, $id)
    {
        $data = $request->all();
        Commission::find($id)->update($data);
        return redirect()->route('commission.index')->with('warning','Komisioni u ndryshua ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $commissions =Commission::find($id);
         $commissions->delete();
         return redirect()->route('commission.index')->with('danger','Komisioni u fshi');
    }
}
