<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Subcategory;
use Gloudemans\Shoppingcart\Facades\Cart;
/*use App\Models\Cart;
use Session;*/

class HauseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       $categories = Category::where('id','1')->get();
       foreach ($categories as $category) {
         $articles = $category->article()->paginate(12);   
         $subcategories = $category->subcategory()->take(6)->get();   
       }

        /*$subs = Subcategory::where('category_id', '2')->take(6)->get();*/
        return view('hause.index', compact(['articles' , 'categories' , 'subcategories']));

    }

     public function show($id)
    {
        $articles = Article::find($id);
        return view('hause.show')->with('article',$articles);
    }

     public function edit($id)
    {
        $article = Article::find($id);
        Cart::add($id, $article->name,1,$article->price , ['image' => $article->image , 'desc' => $article->desc] );

        return back();
    }


  }















// Cart with session method

   /* public function GetAddToCart(Request $request, $id){
        $article = Article::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($article, $article->id);

        $request->session()->put('cart', $cart);
        return redirect('/articles');
    }

    public function getCart(){
        if (!Session::has('cart')){
            return view('shopping-cart', ['articles' => null]);   
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('shopping-cart' , ['articles' => $cart->items, 'totalPrice' =>$cart->totalPrice ]);
       
    }
    public function getReduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->getReduceByOne($id);

        Session::put('cart' , $cart);
        return redirect()->route('articles.shoppingCart');
    }
    public function getRemoveItem($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        if (count($cart->items) > 0) {
        Session::put('cart' , $cart);
        }else{
            Session::forget('cart');
        }

        return redirect()->route('articles.shoppingCart');
    }
 }
*/
