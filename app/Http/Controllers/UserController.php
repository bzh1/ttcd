<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{
     public function edit()
    {       
        if (Auth::user()) {

            $user = User::find(Auth::user()->id);
            if ($user) {
                         
                return view('user.edit')->withUser($user);
                     
            }else{
                return redirect()->back();
            }
        }else{
                return redirect()->back(); 
        }

    }
    public function update(Request $request){
    	$user = User::find(Auth::user()->id);
    	if ($user) {
    		$validate = null;
    		if(Auth::user()->email === $request['email']){
    		$validate = $request->validate([
    		'name' => ['required', 'string', 'max:255' ],
            'email' => ['required', 'string', 'email', 'max:255'],
            'firma' =>['required', 'string', 'max:255'],
            'rruga' =>['required', 'string', 'max:255'],
            'nr'  =>['required','regex:/[0-9]/'],
            'telefon' =>['required','regex:/[0-9]/'],
            'biznesi' =>['required', 'string', 'max:255'],
            'emri' =>['required', 'string', 'max:255'],
            'mbiemri' =>['required', 'string', 'max:255'],
            'emailp' =>['required', 'string', 'email', 'max:255',],
    		]);
    	}else{
    		$validate = $request->validate([
    		'name' => ['required', 'string', 'max:255' ],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'firma' =>['required', 'string', 'max:255'],
            'rruga' =>['required', 'string', 'max:255'],
            'nr'  =>['required','regex:/[0-9]/'],
            'telefon' =>['required','regex:/[0-9]/'],
            'biznesi' =>['required', 'string', 'max:255'],
            'emri' =>['required', 'string', 'max:255'],
            'mbiemri' =>['required', 'string', 'max:255'],
            'emailp' =>['required', 'string', 'email', 'max:255',],
    		]);
    	}

    		if ($validate) {
    			# code...
    		$user->name = $request['name'];
    		$user->email = $request['email'];
			$user->rruga = $request['rruga'];
			$user->nr = $request['nr'];
			$user->telefon = $request['telefon'];
			$user->biznesi = $request['biznesi'];
			$user->emri = $request['emri'];
			$user->mbiemri = $request['mbiemri'];
			$user->emailp = $request['emailp'];

			$user->save();
			$request->session()->flash('success', 'Your details have been updated');
			return redirect()->back();
    		}else{
                return redirect()->back(); 
				    		
    		}		
	}}
}
