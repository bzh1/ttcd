<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ObjektStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'objekt' =>'required',
            'kostoja' => 'required',
            'ort' => 'required',
            'firma'=> 'required', 
            'rruga'=> 'required',
             


        ];
    }
}
