<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'article_name' =>'required',
            'user_name'=>'required',
            'address'=>'required',
            'price'=>'required',
            'qty'=>'required',
            'telefon'=>'required',
        ];
    }
}
