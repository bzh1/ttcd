<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('locale/{locale}', function ($locale){
        Session::put('locale', $locale);
        return redirect()->back();
    });
/*--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------*/
Route::any('/', 'DashboardController@index')->name('Dashboard');
Route::any('/home', 'HomeController@index')->name('Home')->middleware(['auth', 'verified']);
Route::resource('commission','CommissionController')->middleware(['auth', 'verified']);
Route::resource('objekt','ObjectsController')->middleware(['auth', 'verified']);
Route::get('edit/user', 'UserController@edit')->name('user.edit');
Route::post('update/user', 'UserController@update')->name('user.update');
Route::view('/projekt', 'projekt.index');
Route::get('/editori' , function (){ 
	return \File::get(public_path() . '/index.html');
});
Auth::routes(['verify'=>true]);
/*---------------------------S-----------------------------------------------
| Routes For Articles and The Shopping Cart
|--------------------------------------------------------------------------*/
Route::resource('hause', 'HauseController');
Route::resource('buros', 'BuroController');
Route::resource('baus', 'BauController');
Route::resource('search', 'SearchController');
Route::resource('subcategory', 'SubcategoryController');

/*--------------------------------------------------------------------------
| Routes in which you need to be logged in
|--------------------------------------------------------------------------*/
Route::resource('offers', 'OfferController')->middleware(['auth', 'verified']);
Route::resource('cart', 'CartController')->middleware(['auth', 'verified']);
Route::resource('orders', 'OrderController')->middleware('auth');
Route::resource('favorite', 'FavoriteController')->middleware(['auth', 'verified']);
/*--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------*/
Route::name('admin')->namespace('Admin')->prefix('admin')->middleware(['auth','isadmin' , 'verified'])->group(function () {
	Route::get('/', 'HomeController@index')->name('dashboard');
	Route::resource('admin/objekt','ObjectsController');
	Route::resource('admin/commission','CommissionController');
	Route::resource('articles','ArticleController');
	Route::resource('admin/orders','OrderController');
	Route::resource('admin/offers','OfferController');
	Route::resource('admin/subcategory','SubcategoryController');
	Route::resource('admin/user','UserController');
	Route::get('admin/search', 'SearchController@search')->name('search');
});
